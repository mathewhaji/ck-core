# Cobuild Express Stack
Cobuild express stack is a framework from Rokk3r labs to facilite the build project or module of Backend type, also it lets us re-use modules created previously. Cobuild Express Stack has the code to perform the process of adding, updating, deleting and searching data, create users, login and the process if you don’t remember your password. 

**For more info go to [Cobuild wiki](http://wiki.weco.build/categories/Cobuild-Ecosystem/Cobuild-Express-Stack/)**


## Table of contents
* [Install](https://bitbucket.org/rokk3rlabs/cobuild-express-stack#markdown-header-installation)
* [Requirements](https://bitbucket.org/rokk3rlabs/cobuild-express-stack#markdown-header-requirements)
* [Test](https://bitbucket.org/rokk3rlabs/cobuild-express-stack#markdown-header-test) 
* [Version log](https://bitbucket.org/rokk3rlabs/cobuild-express-stack#markdown-header-version-log)

## Installation
Install `eslint` globally to lint in your code. There is an `.eslintrc` file included, but you will need to set up `eslint` for your chosen text editor.
* Clone the repo at https://bitbucket.org/rokk3rlabs/cobuild-express-stack
```
git clone https://bitbucket.org/rokk3rlabs/cobuild-express-stack
```
* Install modules
```
npm install
```
  * Run Server
```
npm start
```

## Requirements
* Node.js 4.X+

## Test
Enter into the browser to test it.  

```
localhost:3000/
```

## Version Log 

### v0.3.1 - **  Jan 26 2017  ###

* New Feature: Signed Upload Url endpoint for secure direct upload to google buckets
* Improvement: New README Content table
* Update: By default use 1 instance, remove instance for each cpu core.


### v0.3.0 - ** [Breaking v0.1.3 and older]** Dec 17 2016  ###

* Improvement: Unit tests for User model
* Bugfix: Unit tests issues
* Bugfix: Twitter auth 
* Upgrade: Auth library
* Upgrade: Cobuild2 library v0.3.0


### v0.2.1 - Nov 17 2016 ###

* Refactor: Auth Module - Cleaned and Organized. Several issues solved
* New Feature: Formular module for CLI Schema helper tools
* New Feature: bitbucket Pipelines



### v0.2.0 - Oct 28 2016 ###

* First Stable Release