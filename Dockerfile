FROM node:8
# Make port and node_env configurable from build command
ARG node_env=production
ARG port=8080
# Set env-vars for container
ENV PORT=$port
ENV NODE_ENV=$node_env
# Copy app 
WORKDIR /app
COPY . /app
# Run npm install (should do nothing, as we already have bitbucket cache)
RUN npm install

CMD npm start

EXPOSE $port