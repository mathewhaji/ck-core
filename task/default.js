var gulp    = require('gulp');

gulp.task('default',function() {
  console.log("\n\n-----------------------------------------------------------------------");
  console.log("\n Gulp tasks available: \n  ");

  console.log(" 'gulp lint' - Code style checker ");
  console.log(" 'gulp apidoc' - Generate API documentation \n ");

  console.log("For testing use Cobuild CLI  ");
  console.log("see --> https://bitbucket.org/rokk3rlabs/cobuild-command-center \n");

  console.log("To run tests: ");
  console.log("   1. Install Cobuild CLI globally ");
  console.log("   2. Go to root of project/module you want to test ");
  console.log("   3. Execute 'cobuild test' \n");
  console.log("-----------------------------------------------------------------------\n\n");
});
