var modulesRoot = '/app/modules';
var routesFolder = 'routes';
var path = require('path');
var Cobuild = require('cobuild2');


module.exports = function modulesLoader(app, limiter) {

    Cobuild.Utils.Files.listFoldersInFolder(modulesRoot)
    .forEach(function (moduleFolder) {
        
        Cobuild.Utils.Files.requireFilesInFolderWithLimiter(path.join(modulesRoot, moduleFolder, routesFolder), app, limiter);
        

    });

};
