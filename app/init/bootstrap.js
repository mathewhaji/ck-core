var async = require('async');
var Cobuild = require('cobuild2');
var _ = require('lodash');
var config = Cobuild.config;
var express = require('express');
var app = express();

var userService    = require(Cobuild.paths.app + '/modules/user/services/userService') ;

var createUser = function(cb) {
  
  userService.create(config.defaultUser)
  .then( function(result) {
    console.log('Bootstrap = ', result);
    cb(null);
  })
  .catch(function(err){
    console.log('Error Bootstrap = ', err);
    cb(err)
  });

};

var timeOut = function(cb){
  setTimeout(function(){cb()},15000)
}

function bootstrap(done) {
  console.log("Going to run bootstrap")

  async.parallel([
    timeOut, 
    createUser
  ],
  function (err, finalCb) {    
    if(typeof done === 'function'){
      done();
    }

  });

};

module.exports = bootstrap;
