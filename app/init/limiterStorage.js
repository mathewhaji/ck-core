module.exports = function(type){
  return {
    'items': {},
    'get': function(key, callback) {
        var item = this.items[key];
        if (item) {
            callback(null, item);
        } else {
            callback('Item not found');
        }
    },
    'set': function(key, value, aux, expire, callback) {
        this.items[key] = value;
        callback(null);
    }
  };

};