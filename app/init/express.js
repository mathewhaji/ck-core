//-------------------------------load modules---------------------------------\\
var express      = require('express');
var mime         = require('mime');
var logger       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');
var i18n         = require('i18n');
var cors         = require('cors');
var Cobuild      = require('cobuild2')
var config       = Cobuild.config
var cluster      = require('cluster');
log              = require('debug')('cobuild-express-stack');
var helmet       = require('helmet');
var multipart = require('connect-multiparty');
var express_limiter = require('express-limiter');

var client        = require(Cobuild.paths.app + '/init/limiterStorage')
const Sentry = require('@sentry/node');

if (config.sentryKey)
  Sentry.init({ dsn: config.sentryKey });

module.exports = function initApp() {

  process.on('uncaughtException', reportError);
  //--------------------setup the express application-------------------------\\
  var app = express();

  var limiter = express_limiter(app, client('local_memory'));
  var expressMiddleware = require('cobuild-express-policy-middleware')
    .expressMiddleware({
      secret: config.jwt.secret,
      service: 'express-server',
      jwtoptions: {algorithms: [config.jwt.algorithm], expiresIn: 18000},//config.jwt.verifyOptions,
      //accessValidation:accessValidation,
      publicActions: { 
        'GetIdentity': true, 
        'GetIdentitie': true, 
        'ListPublickeys': true, 
        'GetPublic': true, 
        'GetApi': true, 
        'CreateUser':true, 
        'ListReports': true, 
        'GetReport': true, 
        'GetCompanie': true, 
        'CreateAuth': true  
      }
    });

  i18n.configure({
    locales: ['en', 'es'],
    defaultLocale: 'en',
    directory: __dirname + '/locales'
  });



  app
    .use(logger('dev'))            //logs every request to console
    .use(helmet())
    .use(cookieParser())            //use cookie - needed for auth
    .use(bodyParser.json())         // to support JSON-encoded bodies
    .use(bodyParser.urlencoded({    // to support URL-encoded bodies
      extended: false
    }))
    .use(multipart({
      uploadDir: '/tmp/'
    }))
    .use(session({                  //configures sessions setting
      secret: config.web.sessionSecret,
      resave: false,
      saveUninitialized: true
    }))
    .use(function(req, res, next) {
      config.web.host = req.headers.host;
      next();
    })
    .use(express.static(Cobuild.Utils.Files.resolvePath('public')))
    .use(i18n.init)
    .use(cors({credentials:true, origin:true}))
    .use(expressMiddleware);

  app.disable('x-powered-by');

  
  //--------------------------initialize custom responses---------------------\\
  require('express-custom-response')(Cobuild.paths.customResponses);

  //--------------------------------Load modules-------------------------------\\
  require(Cobuild.paths.app + '/init/modulesLoader')(app, limiter);

  //-----------------------------start server---------------------------------\\
  var server = app.listen(config.web.port, function() {
    var port = server.address().port;
    console.log('Server is running on port: ' , port, "  and worker: ",(cluster.worker ? cluster.worker.id : 'n/a'));
    return server;
  });

  function reportError(err, req)  {
    console.log('============uncaughtException===============')

    console.log( err.message );
    console.log( err.stack );
    

  }
  
};
