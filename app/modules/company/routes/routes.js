var Cobuild           = require('cobuild2');
var express           = require('express');
var router            = express.Router();
var companyController   = require(Cobuild.Utils.Files.dirpath(__dirname)+'/controllers/companyController');
var companyValidator    = require(Cobuild.Utils.Files.dirpath(__dirname)+'/validators/company');



module.exports = function(app, limiter) {

  /**
  *   Limiter
  *     For more info see: https://www.npmjs.com/package/express-limiter
  */

  if (limiter){
    // Here insert the rules for use limiter.
    // Example
    limiter({
      path: '/api/'+Cobuild.config.apiVersion+'/companies/health',
      method: 'get',
      lookup: ['connection.remoteAddress'],
      // 10 requests per 10 seconds
      total: 10,
      expire: 1000 * 10 ,
      onRateLimited: function (req, res, next) {
        next({ message: 'Rate limit exceeded', status: 400 })
      }
    });
  } 

  /**
  * The Policies load when init a server, in that policies are some validations like:
  * isLoggedIn or isAdmin
  * To use:
  * 
  * var authPolicyIsLoggedIn      = Cobuild.Policies.getPolicy('cobuild.'+config.authentication.strategy+'.isLoggedIn');
  * var authPolicyIsAdmin      = Cobuild.Policies.getPolicy('cobuild.'+config.authentication.strategy+'.isAdmin');
  * 
  * On the declaretion of end-point call a policy
  *
  * router.put('/', authPolicyIsLoggedIn, Ctrl.update);
  *
  */
  
	router.get('/health', function(req, res) {
    
     return res.status(200).send('ok');
  });

/**
 * @api {post} /api/v1/companies Create a Company
 * @apiName Company - Create
 * @apiGroup Company
 * @apiDescription Create a Company into the database
 * @apiVersion 1.0.0
 *
 * @apiParam {String} title A random title param of this record
 *
 * @apiParamExample {json} Request-Example:
 * {
 *   "title": "Hello world"
 * }
 *
 * @apiSuccessExample {JSON} response-example:
 * {
 *   "__v": 0,
 *   "title": "Hello world",
 *   "_id": "573f2904e383af8c2e612b5e"
 * }
 */  
  router.post('/', companyController.create);

/**
 * @api {post} /api/v1/companies Get company by id
 * @apiName Company - Get
 * @apiGroup Company
 * @apiDescription Get company by id
 * @apiVersion 1.0.0
 *
 * @apiParam {String} id company's id 
 *
 *
 * @apiSuccessExample {JSON} response-example:
 * {
 *   "__v": 0,
 *   "name": "Hello world",
 *   "logo": "https://storage.cloud.google.com/cytrust-assets/000a698b-7b58-b2b4-7172-93d5b69ad9fe.png"
 * }
 */
  router.get('/:id', companyController.getCompany)

  router.get('/users/:role', companyController.listCompanyUsers); 

  console.log('/api/'+Cobuild.config.apiVersion+'/companies');
  app.use('/api/'+Cobuild.config.apiVersion+'/companies',router);

  

}