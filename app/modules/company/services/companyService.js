var Cobuild           = require('cobuild2');
var _                 = require('lodash');
var async             = require('async');
var Company           = require(Cobuild.Utils.Files.dirpath(__dirname)+'/models/company');
var UsersCompany      = Cobuild.Utils.Files.requireIfExists('/app/modules/user/models/userCompany')

var request           = require('request-promise');
var bcrypt            = require('bcrypt-nodejs');
var i18n              = require('i18n')

var userService       = Cobuild.Utils.Files.requireIfExists('/app/modules/user/services/userService');
var exports           = module.exports;



exports.create = function(data){
  return new Promise(function(resolve, reject){
      var createCompany = function(cb){
        var company = _.pick(data, ['name', 'logo_url']);

        Company('company')
        .insert(company,'*')
        .then(function(result) {
          let company = result.shift()
          cb(null, company);
        })
        .catch(cb);
      }


      var createOwnerUser = function(cbData, cb){
        var companyId = cbData.id;

        var user = _.pick(data, ['lastName', 'firstName', 'email']);
        user.role = 'owner';

        userService.createUserCompany(companyId, user)
        .then((response)=>{
          cb(null, cbData)
        })
        .catch(cb)
      }

      var tasks = [
        createCompany, 
        createOwnerUser
      ]

      async.waterfall(tasks, (err, response)=>{
        if(err) return reject(err);

        resolve(response)
      })
  })
};

exports.getCompany = function getCompany(id){
 return new Promise(function(resolve, reject){
    Company
    .from('company')
    .where({id:id})
    .first()
    .then((response)=>{
      resolve(response);
    })
    .catch((err) =>{
      reject(err);
    })
  });
}

exports.list = function list(requestData){

  var pagination = {};
  
  if(requestData.query.limit){
    pagination.limit = parseInt(requestData.query.limit)
    pagination.page = parseInt(requestData.query.page) || 1
    pagination.offset = (pagination.page - 1) * pagination.limit;

    delete requestData.query.limit;
    delete requestData.query.page;
  }


  var query = Cobuild.Utils.Request.formatQueryString(requestData.query);
  var sort = Cobuild.Utils.Request.formatSortString(requestData.query.sort);

  if(!_.isEmpty(sort)){
    pagination.sort = sort;
    delete requestData.query.sort;
  }
  
  var id = requestData.params.id || requestData.query.id;
  
  if(id !== undefined) {
    query.id = id;
  }


  query.role = requestData.params.role;


  return new Promise(function(resolve, reject){
    
    var count = UsersCompany
                .count('*')
                .from('user_company')
                .where(query)
                .first();


    var find = UsersCompany
                .select([
                  'user.*',
                  'company.*'
                ])
                .from('user_company')
                .innerJoin('user', 'user.id', 'user_company.user')
                .innerJoin('company', 'company.id', 'user_company.company')
                .where(query)

    if(!_.isEmpty(pagination)){
      find
      .limit(pagination.limit)
      .offset(pagination.offset)
    }

    if(pagination.sort){
      find
      .orderBy(pagination.sort);
    }

    Promise.all([count, find])
    .then(function(values){

      var paging = {
        totalItems: _.isEmpty(values[0]) ? 0 : values[0].count,
        limit: parseInt(pagination.limit) || -1,
        page: pagination.page
      };

      paging.totalPages = Math.ceil(paging.totalItems / paging.limit);

      resolve({data:values[1], paging:paging});

    })
    .catch(reject);

  });
  

};