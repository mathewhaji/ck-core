var Cobuild                     = require('cobuild2');
var _                           = require('lodash');
var companyService              = require(Cobuild.Utils.Files.dirpath(__dirname)+'/services/companyService');

var exports                     = module.exports;

exports.create = function(req, res) {
  
  companyService.create(req.body)
  .then(function(record) {
    res.ok(record)
  })
  .catch(function(err){
    res.badRequest(err);
  });

};

exports.listCompanyUsers = function(req, res){

  var id = req.params.id || req.query.id;

  companyService
  .list(req)
  .then(
    
    function success(result){

      if(id !== undefined){
        res.ok((result && result.data.length > 0)? result.data[0] : {});
      }else{
        res.ok(result);  
      }
    },

    function error(err){
      res.badRequest(err);
    });
  
}

exports.getCompany = function (req, res){
  var id = req.params.id;

  companyService
  .getCompany(id)
  .then(function(response){
    res.ok(response)
  })
  .catch(function(err){
    res.badRequest(err);
  })
}
