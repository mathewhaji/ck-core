var Cobuild           = require('cobuild2');
var express           = require('express');
var router            = express.Router();
var risks                        = require(Cobuild.Utils.Files.dirpath(__dirname)+'/controllers/riskController');
var risksValidator               = require(Cobuild.Utils.Files.dirpath(__dirname)+'/validators/risk');



module.exports = function(app, limiter) {

  /**
  *   Limiter
  *     For more info see: https://www.npmjs.com/package/express-limiter
  */

  if (limiter){
    // Here insert the rules for use limiter.
    // Example
    limiter({
      path: '/api/'+Cobuild.config.apiVersion+'/risks/health',
      method: 'get',
      lookup: ['connection.remoteAddress'],
      // 10 requests per 10 seconds
      total: 10,
      expire: 1000 * 10 ,
      onRateLimited: function (req, res, next) {
        next({ message: 'Rate limit exceeded', status: 400 })
      }
    });
  }

  /**
  * The Policies load when init a server, in that policies are some validations like:
  * isLoggedIn or isAdmin
  * To use:
  *
  * var authPolicyIsLoggedIn      = Cobuild.Policies.getPolicy('cobuild.'+config.authentication.strategy+'.isLoggedIn');
  * var authPolicyIsAdmin      = Cobuild.Policies.getPolicy('cobuild.'+config.authentication.strategy+'.isAdmin');
  *
  * On the declaretion of end-point call a policy
  *
  * router.put('/', authPolicyIsLoggedIn, Ctrl.update);
  *
  */

	router.get('/health', function(req, res) {

     return res.status(200).send('ok');
  });

 /**
 * @api {get} /api/v1/risks/survey/:id  Get survey results
 * @apiName Core - Get survey results
 * @apiGroup Risks
 * @apiDescription Get survey results
 * @apiVersion 1.0.0
 *
 * @apiSuccessExample {JSON} response-example:*
 *
 *  {
 *    "data": {
 *      "id": 549,
 *      "company_name": null,
 *      "created_at": "2017-12-07T15:41:05.260Z",
 *      "questions": {
 *        "basic,regulatory_organism": "FCA",
 *        "basic,dedicated_security": "Y",
 *        "basic.partners": "Betweeen 10 - 50",
 *        "basic.employees": "Betweeen 0-10",
 *        "basic,staff": "Y",
 *        "basic,type": "Bespoke",
 *        "basic,sensitive_data": "N",
 *        "basic,share_data": "user",
 *        "basic.customers": "Betweeen 1000 - 10000",
 *        "advanced,standards": "ISO27001",
 *        "advanced,patch": "Y",
 *        "advanced,infrastructure": "Cloud",
 *        "advanced,controls": "Restricted Access",
 *        "advanced,audit_clients": "Y",
 *        "advanced,customer_auth": "Y",
 *        "advanced,iam": "Y",
 *        "advanced,security_monitoring": "Web - customer",
 *        "advanced.security_monitoring": "Web - customer"
 *      },
 *      "email": null,
 *      "scores": {
 *        "advanced": {
 *          "Data Breach": 15,
 *          "DDoS": 8,
 *          "GDPR": 12,
 *          "Insider Threat": 8,
 *          "S/W Vuln.": 7
 *        },
 *        "basic": {
 *          "Data Breach": 24,
 *          "DDoS": 16,
 *          "GDPR": 22,
 *          "Insider Threat": 21,
 *          "S/W Vuln.": 18
 *        },
 *        "overall": {
 *          "Data Breach": 39,
 *          "DDoS": 24,
 *          "GDPR": 34,
 *          "Insider Threat": 29,
 *          "S/W Vuln.": 25
 *        }
 *      }
 *    }
 *  }
 */
  router.get('/survey/:id', risks.calculateSurveyScore);

  /**
 * @api {get} /api/v1/risks/thresholds/overall  Get overall thresholds
 * @apiName Core -  Get overall thresholds
 * @apiGroup Risks
 * @apiDescription  Get overall thresholds
 * @apiVersion 1.0.0
 *
 * @apiSuccessExample {JSON} response-example:*
 *
 *  {
 *    "data": {
 *      "Data Breach": {
 *          "L": 38,
 *          "M": 52,
 *          "H": 77
 *      },
 *      "Insider Threat": {
 *          "L": 28,
 *          "M": 39,
 *          "H": 56
 *      },
 *      "GDPR": {
 *          "L": 32,
 *          "M": 44,
 *          "H": 64
 *      },
 *      "S/W Vuln.": {
 *          "L": 30,
 *          "M": 35,
 *          "H": 44
 *      },
 *      "DDoS": {
 *          "L": 33,
 *          "M": 39,
 *          "H": 52
 *      }
 *    }
 *  }
 */
  router.get('/thresholds/overall', risks.overallScore);


 /**
 * @api {get} /api/v1/risks/:form  Get risk score for a given form
 * @apiName Core -  Get thresholds score for a given form
 * @apiGroup Risks
 * @apiDescription  Get thresholds score for a given form
 * @apiVersion 1.0.0
 *
 * @apiParam {string} form type basic/advanced
 *
 * @apiSuccessExample {JSON} response-example:*
 *
 *  {
 *    "data": {
 *      "Data Breach": {
 *        "L": 23,
 *        "M": 29,
 *        "H": 45
 *      },
 *      "Insider Threat": {
 *        "L": 18,
 *        "M": 24,
 *        "H": 36
 *      },
 *      "GDPR": {
 *        "L": 18,
 *        "M": 26,
 *        "H": 40
 *      },
 *      "S/W Vuln.": {
 *        "L": 16,
 *        "M": 16,
 *        "H": 22
 *      },
 *      "DDoS": {
 *        "L": 17,
 *        "M": 23,
 *        "H": 29
 *      }
 *    }
 *  }
 */
  router.get('/:form', risks.calculateRisk);

  /**
 * @api {get} /api/v1/risks/results/:id  Store survey results - TO BE DEPRECATED, INSTEAD USE survey-results/:id
 * @apiName Core -  Store survey results
 * @apiGroup Risks
 * @apiDescription  Store survey results
 * @apiVersion 1.0.0
 *
 * @apiParam {integer} id id of the survey
 *
 * @apiSuccessExample {JSON} response-example:*
 *
 *  {
 *    "data": {
 *      "id": 25,
 *      "created_at": "2017-12-12T21:32:06.536Z",
 *      "survey": 555
 *      "scores": {
 *        "basic": [
 *          {
 *            "Data Breach": {
 *              "value": 26,
 *              "level": "M"
 *            }
 *          },
 *          {
 *            "DDoS": {
 *              "value": 16,
 *              "level": "L"
 *            }
 *          },
 *          {
 *            "GDPR": {
 *              "value": 23,
 *              "level": "M"
 *            }
 *          },
 *          {
 *            "Insider Threat": {
 *              "value": 20,
 *              "level": "M"
 *            }
 *          },
 *          {
 *            "S/W Vuln.": {
 *              "value": 10,
 *              "level": "L"
 *            }
 *          }
 *      ],
 *      "overall": [
 *          {
 *            "Data Breach": {
 *              "value": 26,
 *              "level": "L"
 *            }
 *          },
 *          {
 *            "DDoS": {
 *              "value": 16,
 *              "level": "L"
 *            }
 *          },
 *          {
 *            "GDPR": {
 *              "value": 23,
 *              "level": "L"
 *            }
 *          },
 *          {
 *            "Insider Threat": {
 *              "value": 20,
 *              "level": "L"
 *            }
 *          },
 *          {
 *            "S/W Vuln.": {
 *              "value": 10,
 *              "level": "L"
 *            }
 *          }
 *        ]
 *      }
 *    }
 *  }
 */
router.get('/results/:id', risks.surveyResults);

/**
 * @api {get} /api/v1/risks/survey-results/:id
 * @apiName Core -  Calculate survey results
 * @apiGroup Risks
 * @apiDescription  Calculate survey results
 * @apiVersion 1.0.0
 *
 * @apiParam {integer} id id of the survey
 *
 * @apiSuccessExample {JSON} response-example:*
 *
 * {
 *   "data": {
 *     "risk": {
 *       "score": 180,
 *       "level": "medium"
 *     },
 *     "company": "keily.salinas@rokk3rlabs.com",
 *     "categories": [
 *       {
 *         "category": "DDoS",
 *         "level": "H"
 *       },
 *       {
 *         "category": "Insider Threat",
 *         "level": "H"
 *       },
 *       {
 *         "category": "Data Breach",
 *         "level": "H"
 *       },
 *       {
 *         "category": "GDPR",
 *         "level": "M"
 *       },
 *       {
 *         "category": "S/W Vuln.",
 *         "level": "H"
 *       },
 *       {
 *         "category": "E-Theft",
 *         "level": "H"
 *       }
 *     ],
 *     "surveyType": "basic",
 *     "chartLimits": {
 *       "low": 300,
 *       "medium": 270,
 *       "high": 170
 *     }
 *   }
 * }
 */
router.get('/survey-results/:id', risks.results);

 /**
 * @api {get} /api/v1/risks/list/:id  Get survey scores
 * @apiName Core -  Get survey scores
 * @apiGroup Risks
 * @apiDescription  Get survey scores
 * @apiVersion 1.0.0
 *
 * @apiParam {integer} id id of the survey
 *
 * @apiSuccessExample {JSON} response-example:*
 *
 *  {
 *    "data": {
 *      "id": 25,
 *      "created_at": "2017-12-12T21:32:06.536Z",
 *      "survey": 555
 *      "scores": {
 *        "basic": [
 *          {
 *            "Data Breach": {
 *              "value": 26,
 *              "level": "M"
 *            }
 *          },
 *          {
 *            "DDoS": {
 *              "value": 16,
 *              "level": "L"
 *            }
 *          },
 *          {
 *            "GDPR": {
 *              "value": 23,
 *              "level": "M"
 *            }
 *          },
 *          {
 *            "Insider Threat": {
 *              "value": 20,
 *              "level": "M"
 *            }
 *          },
 *          {
 *            "S/W Vuln.": {
 *              "value": 10,
 *              "level": "L"
 *            }
 *          }
 *      ],
 *      "overall": [
 *          {
 *            "Data Breach": {
 *              "value": 26,
 *              "level": "L"
 *            }
 *          },
 *          {
 *            "DDoS": {
 *              "value": 16,
 *              "level": "L"
 *            }
 *          },
 *          {
 *            "GDPR": {
 *              "value": 23,
 *              "level": "L"
 *            }
 *          },
 *          {
 *            "Insider Threat": {
 *              "value": 20,
 *              "level": "L"
 *            }
 *          },
 *          {
 *            "S/W Vuln.": {
 *              "value": 10,
 *              "level": "L"
 *            }
 *          }
 *        ]
 *      }
 *    }
 *  }
 */
  router.get('/list/:id', risks.list);

  router.get('/monetary/:id', risks.monetary);

  console.log('/api/'+Cobuild.config.apiVersion+'/risks');
  app.use('/api/'+Cobuild.config.apiVersion+'/risks',router);

}