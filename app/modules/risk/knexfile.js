// Update with your config settings.

module.exports = {

  development: {
    client: process.env.DB_TYPE,
    connection: process.env.DB_URL,
    pool: { 
      min: 0, 
      max: process.env.DB_MAX_CONNECTIONS || 6 
    }
  },

  staging: {
    client: process.env.DB_TYPE,
    connection: process.env.DB_URL,
    pool: { 
      min: 0, 
      max: process.env.DB_MAX_CONNECTIONS || 6 
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  production: {
    client: process.env.DB_TYPE,
    connection: process.env.DB_URL,
    pool: { 
      min: 0, 
      max: process.env.DB_MAX_CONNECTIONS || 6 
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  }

};
