'use strict';
/* jshint undef: false */
/* jshint unused: false */
/* global describe it before async f request */

if(!process.env.NODE_PATH){
  throw new Error("NODE_PATH environment variable is required");
}

var Cobuild         = require('cobuild2');
var fixtures        = Cobuild.Utils.Test.loadFixtures(__dirname);
var request         = require('supertest');
var request         = request('http://localhost:3000');
var should          = require('should');

//console.log('fixtures',fixtures)

describe('Core Tests', function() {

  it('should be able to reach health endpoint', function(done) {
    this.timeout(10000);

    request
      .get('/api/'+Cobuild.config.apiVersion+'/risks/health')
      .expect(200)
      .end(done);
  });

  it('should be able to calculate survey score', function(done) {
    this.timeout(10000);

    request
      .get('/api/'+Cobuild.config.apiVersion+'/risks/survey/549')
      .expect(200)
      .end(function(err, res){
        should.not.exist(err);
        res.body.should.have.property('data');
        console.log('Risk Survey', res.body.data);
        done(err);
      });
  });

  it('should be able to calculate overall threshold', function(done) {
    this.timeout(10000);

    request
      .get('/api/'+Cobuild.config.apiVersion+'/risks/thresholds/overall')
      .expect(200)
      .end(function(err, res){
        should.not.exist(err);
        res.body.should.have.property('data');
        console.log('Overall score', res.body.data);
        done(err);
      });
  });

  it('should be able to calculate form threshold', function(done) {
    this.timeout(10000);

    var form = 'basic';

    request
      .get('/api/'+Cobuild.config.apiVersion+'/risks/'+ form)
      .expect(200)
      .end(function(err, res){
        should.not.exist(err);
        res.body.should.have.property('data');
        console.log( form + 'score', res.body.data);
        done(err);
      });
  });

  it('should be able to calculate survey results', function(done) {
    this.timeout(10000);

    var survey = 549;

    request
      .get('/api/'+Cobuild.config.apiVersion+'/risks/results/'+ survey)
      .expect(200)
      .end(function(err, res){
        should.not.exist(err);
        res.body.should.have.property('data');
        console.log('Survey results', res.body.data);
        done(err);
      });
  });

  it('should be able to validate algorithm calculation', function (done) {
    this.timeout(10000);

    var form = 'basic';
    var level = ['L', 'M', 'H'];
    var category = ['Data Breach', 'Insider Threat', 'GDPR', 'S/W Vuln.', 'DDoS'];

    request
      .get('/api/' + Cobuild.config.apiVersion + '/risks/' + form)
      .expect(200)
      .end(function (err, res) {
        should.not.exist(err);
        console.log('Form:', form, 'Category:', category[1], 'Level:', level[2], 'Risk:', fixtures.formTest.data[category[1]][level[2]] , 'Response:', res.body.data[category[1]][level[2]]);
        
        res.body.data[category[1]][level[2]].should.be.equal(fixtures.formTest.data[category[1]][level[2]]);
        done(err);
      });
  });

});