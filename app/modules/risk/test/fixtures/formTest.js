module.exports = {
  data: {
    'Data Breach': {
      L: 23,
      M: 29,
      H: 45
    },
    'Insider Threat': {
      L: 18,
      M: 24,
      H: 36
    },
    'GDPR': {
      L: 18,
      M: 26,
      H: 40
    },
    'S/W Vuln.': {
      L: 16,
      M: 16,
      H: 22
    },
    'DDoS': {
      L: 17,
      M: 23,
      H: 29
    }
  }
};