# Cobuild Express Boilerplate module

The purpose of this repository is provide a clear understanding of the structure, and outline of a Cobuild express module, as well as provide some very basic CRUD operations out of the box. This module can be dropped in the *app/modules* folder of any derivated repository from the [Cobuild Express Stack](https://bitbucket.org/rokk3rlabs/cobuild-express-stack)

**For more info go to [Cobuild wiki](http://wiki.weco.build/2017/02/01/Cobuild-Ecosystem/Cobuild-Express-Stack/Cobuild-ExpressBoilerplate-Module/)**

## Table of contents
* [Installation](https://bitbucket.org/rokk3rlabs/cobuild-express-module-boilerplate#markdown-header-installation)
* [Requirements](https://bitbucket.org/rokk3rlabs/cobuild-express-module-boilerplate#markdown-header-Requirements)

## Installation

[Cobuild Module Boilerplate](https://bitbucket.org/rokk3rlabs/cobuild-module-boilerplate) requires [Node.js](https://nodejs.org/) v4+,  [Mongo DB](https://www.mongodb.com/) and [Mocha](https://mochajs.org/) installed on your local machine to run. To check it out and use it standalone, just clone it on your local machine doing:

```sh
$ git clone https://bitbucket.org/rokk3rlabs/cobuild-module-boilerplate
```
Install the required dependencies

```sh
$ npm install
```
And to see working the CRUD basic operations working just run the mocha tests doing:

```sh
$ mocha
```

## Requirements 

* Node.js 4.X+  

***
### Version
1.0.0

License
----
ISC