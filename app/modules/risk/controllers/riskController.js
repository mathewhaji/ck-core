var Cobuild                     = require('cobuild2');
var _                           = require('lodash');
var riskService                 = require(Cobuild.Utils.Files.dirpath(__dirname)+'/services/riskService');

var exports                     = module.exports;

exports.calculateSurveyScore = function(req, res) {
  riskService
  .calculateScoresValue(req)
  .then(function (result) {
    res.ok(result);
  })
  .catch(function(err){
    res.badRequest(err);
  });

};

exports.surveyResults = function(req, res) {
  riskService
  .surveyResults(req)
  .then(function (result) {
    res.ok(result);
  })
  .catch(function(err){
    res.badRequest(err);
  });
};

exports.results = function(req, res) {
  riskService
  .results(req)
  .then(function (result) {
    res.ok(result);
  })
  .catch(function(err){
    res.badRequest(err);
  });
};

exports.calculateRisk = function(req, res) {
  
  var form = req.params.form

  if(_.isUndefined(form) || _.isEmpty(form))
    res.badRequest('Missing form parameter');

  riskService.calculateRisk({ form: form })
    .then(result => {
      res.ok(result)
    })
    .catch(function (err) {
      res.badRequest(err);
    });

};

exports.overallScore = function(req, res) {
  
  riskService.calculateOverallScore()
    .then( result => {
      res.ok(result)
    })
    .catch(function (err) {
      res.badRequest(err);
    });

};

exports.list = function (req, res) {

  var id = req.params.id || req.query.id;

  riskService
    .getScore(req)
    .then(result => {
      res.ok(result);
    })
    .catch(function (err) {
      res.badRequest(err);
    });

};

exports.monetary = function(req, res) {
  riskService.monetaryRisk(req)
    .then( result => {
      res.ok(result)
    })
    .catch(function (err) {
      res.badRequest(err);
    });

};

