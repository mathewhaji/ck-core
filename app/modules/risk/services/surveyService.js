var Cobuild = require('cobuild2');
var request = require("request")
var config = Cobuild.config;

const updateSurvey = (data, id) => {

  let req = {
    method: 'PUT',
    url: `http://survey:1337/api/v1/surveys/${id}`,
    json: { scores: data },
    headers: {
      'Authorization':'Bearer ' + config.iam.adminToken
    }
  }

  return new Promise((resolve, reject) => {
    request(req, (err, response, body) => {
      if (!err)
        return resolve({ data: body });
      return reject(new Error('Error updating survey'));
    })
  })
};

const getSurveyData = (id) => {

  let req = {
    url: `http://survey:1337/api/v1/surveys/${id}`,
    json: true,
    headers: {
      'Authorization':'Bearer ' + config.iam.adminToken
    }
  }

  return new Promise((resolve, reject) => {
    request(req, (err, response, body) => {
      if (!err)
        return resolve({ data: body });
      return reject(new Error('Error requesting Survey Data'));
    })
  })
};

const getCategoriesLevels = (id) => {

  let req = {
    url: `http://survey:1337/api/v1/surveys/${id}/categories-levels`,
    json: true,
    headers: {
      'Authorization':'Bearer ' + config.iam.adminToken
    }
  }

  return new Promise((resolve, reject) => {
    request(req, (err, response, body) => {
      if (!err)
        return resolve(body.data);
      return reject(new Error('Error requesting Survey Data'));
    })
  })
};

const getSurveyRiskScore = (id) => {

  let req = {
    url: `http://survey:1337/api/v1/surveys/${id}/cyberscore`,
    json: true,
    headers: {
      'Authorization':'Bearer ' + config.iam.adminToken
    }
  }

  return new Promise((resolve, reject) => {
    request(req, (err, response, body) => {
      if (!err)
        return resolve(body.data);
      return reject(new Error('Error requesting Survey Data'));
    })
  })
};

const getForm = (form) => {

  let req = {
    url: `http://survey:1337/api/v1/surveys/form?questionnaire=${form}`,
    json: true,
    headers: {
      'Authorization':'Bearer ' + config.iam.adminToken
    }
  }

  return new Promise((resolve, reject) => {
    request(req, (err, response, body) => {
      if (!err)
        return resolve(body.data);
      return reject(new Error('Error requesting Survey Data'));
    })
  })
};

const getSurveyScore = (id) => {

  let req = {
    url: `http://survey:1337/api/v1/surveys/${id}/score`,
    json: true,
    headers: {
      'Authorization':'Bearer ' + config.iam.adminToken
    }
  }

  return new Promise((resolve, reject) => {
    request(req, (err, response, body) => {
      if (!err)
        return resolve({ data: body });
      
      console.log('getSurveyScore:', err );
      return reject(new Error('Error requesting Survey Scores'));
    })
  })
};

module.exports = {
  getForm,
  updateSurvey,
  getSurveyData,
  getSurveyScore,
  getSurveyRiskScore,
  getCategoriesLevels,
};