var Cobuild = require('cobuild2');
var request = require("request")
var config = Cobuild.config;

const getSummaryRisk = (payload) => {

  let req = {
    url: `http://calculator:1337/api/v1/risks/summary`,
    json: true,
    headers: {
      'Authorization':'Bearer ' + config.iam.adminToken
    },
    method: 'POST',
    body: payload
  }

  return new Promise((resolve, reject) => {
    request(req, (err, response, body) => {
      if (!err)
        return resolve({ data: body });
      
      console.log('getSummaryRisk:', err );
      return reject(new Error('Error requesting Survey Scores'));
    })
  })
};

const getMonetaryRisk = (payload) => {

  let req = {
    url: `http://calculator:1337/api/v1/risks/monetary`,
    json: true,
    headers: {
      'Authorization':'Bearer ' + config.iam.adminToken
    },
    method: 'POST',
    body: payload
  }

  return new Promise((resolve, reject) => {
    request(req, (err, response, body) => {
      if (!err)
        return resolve({ data: body });
      
      console.log('getMonetaryRisk:', err );
      return reject(new Error('Error requesting Survey Scores'));
    })
  })
};

module.exports = {
  getSummaryRisk,
  getMonetaryRisk
};