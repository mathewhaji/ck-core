var Cobuild = require('cobuild2');
var _ = require('lodash');
var async = require('async');
var request = require("request")
var Risk = require(Cobuild.Utils.Files.dirpath(__dirname) + '/models/risk');
var SurveyService = require(Cobuild.Utils.Files.dirpath(__dirname) + '/services/surveyService');
var CalculatorService = require(Cobuild.Utils.Files.dirpath(__dirname) + '/services/calculatorService');
var config = Cobuild.config;
var fs = require('fs');
var moment = require('moment');

let form = {};
const types = ['L', 'M', 'H'];
const categoryItems = ['Data Breach', 'Insider Threat', 'GDPR', 'S/W Vuln.', 'DDoS', 'Spear-Phishing'];

function getQuestionsScores() {

  let req = {
    url: config.surveyFormURL,
    json: true,
    headers: {
      'Authorization':'Bearer ' + config.iam.adminToken
    }
  }

  return new Promise((resolve, reject) => {
    request(req, (err, response, body) => {
      if (!err)
        return resolve({ data: body });
      return reject(new Error('Error requesting Scores JSON'));
    })
  })
};

// create categories groups
function generateScoreGroup(params) {
  let scores = [];
  params.subsections[0].questions.forEach(element => {
    element.question.answers.forEach(item => {
      if (item.type === undefined) {
        let subScore = [];
        item.forEach(group => {
          if(group.options){

            group.options.forEach((i, j) => {
              if(i.scores){
                i.scores.forEach(value => {
                  value.slug = element.question.slug
                })
              }
              
            });
            subScore = _.flatten(subScore.concat(group.options.map(e => e.scores)));
          }
        });

        scores = scores.concat(subScore);
      }
      else {
        if (item.scores) {
          item.scores.forEach(e => {
            e.slug = element.question.slug
          })
        }
        scores = _.filter(scores.concat(item.scores), undefined)
      }
    });
  })
  return _.groupBy(scores, 'category');
}

function init() {
  return new Promise((resolve, reject) => {
    if (_.isEmpty(form)) {
      getQuestionsScores()
        .then(res => {
          form = {
            basic: generateScoreGroup(_.head(res.data.data)),
            advanced: generateScoreGroup(_.last(res.data.data))
          }
          resolve(form);
        })
        .catch(err => {
          reject(err)
        });
    }
    else
      resolve(form);
  });

}

// group levels
function groupLevels(options, type, group) {
  let risk = 0;

  let currentType = _.findIndex(types, (val) => val === type);
  let subs = [];
  let slugSum = 0;
  let slugGroup = _.groupBy(options, 'slug');
  let validation = [];
  _.forEach(slugGroup, (items, i) => {
    let option = _.find(items, { level: type });
    if (option) {
      risk += parseInt(option.weight);
    }
    else {
      option = _.filter(items, function (value) {
        return value.level.indexOf(type) !== -1 && value.level.indexOf('-') !== -1;
      });
      if (option.length) {
        let maxValue = _.maxBy(option, 'weight');
        risk += parseInt(maxValue.weight);
      }
      else {
        let nextLevel;
        let currentLevel = _.clone(currentType);
        let newType = _.clone(type);
        let countTypes = 'high';
        let count = 1;
        let slugGroupMap = _.clone(items);
        types.forEach((value) => {
          if (!nextLevel) {
            if (types[currentLevel + 1] && countTypes === 'high') {
              currentLevel = currentLevel + 1;
              newType = types[currentLevel];
              countTypes = 'high';
              count = count === 1 ? count : count + 1;
            }
            else if (types[currentLevel - count]) {
              currentLevel = currentLevel - count;
              newType = types[currentLevel];
              countTypes = 'less';
              count = 1;

            }
            let findNewType = _.find(slugGroupMap, function (value) {
              return value.level.indexOf(newType) !== -1;
            });
            if (findNewType) {
              let order = countTypes === 'high' ? 'asc' : 'desc';
              slugGroupMap = _.orderBy(slugGroupMap, ['weight'], [order]);
              let getValue = _.find(slugGroupMap, (value) => {
                return value.level.indexOf(newType) !== -1;
              });
              nextLevel = newType;
              risk += parseInt(getValue.weight);
            }
          }
        });
      }
    }
  });
  return risk;
}

// calculate the thresholds Basic and advanced survey
function calculateRisk(data) {
  return new Promise((resolve, reject) => {
    init().then((form) => {
      let counts = {};
      let selectedForm = form[data.form];

      _.forEach(selectedForm, (item, key) => {
        counts[key] = {};
        types.forEach(type => {
          counts[key][type] = groupLevels(selectedForm[key], type, []);
        })
      });

      resolve(counts);
    })
      .catch(reject);
  });
}

// sum all risks
function sumRisks(risks) {

  let sum = {}
  let counts = groupFormScores(risks);

  _.forEach(counts, (value, key) => {
    types.forEach(type => {
      sum[key] = sum[key] || {}
      sum[key][type] = _.sumBy(value, function (o) { return o[type] });
    })
  })

  return sum;
}

// group forms risks by category
function groupFormScores(risks) {

  let data = [];

  _.forEach(risks, (item) => {
    _.forEach(item, (val, key) => {
      let res = {}
      res[key] = val
      data.push(res)
    })
  })

  var values = _.map(categoryItems, key => _(data).map(key).compact().value());
  var result = _.zipObject(categoryItems, values);

  return result;
}

// calculate the overall threshold values,
function calculateOverallScore() {

  let counts = {};
  let formCounts = [];
  let overall;

  return new Promise((resolve, reject) => {
    init().then((form) => {
      _.forEach(form, (item, key) => {
        _.forEach(form[key], (value, res) => {
          counts[res] = {};
          types.forEach(type => {
            counts[res][type] = groupLevels(form[key][res], type, []);
          })
        });
        formCounts.push(_.clone(counts));
      });

      overall = sumRisks(formCounts);

      resolve(overall);
    })
      .catch(reject)
  })
}

function calculateScoresValue(requestData) {

  var query = Cobuild.Utils.Request.formatQueryString(requestData.params);
  return new Promise(function (resolve, reject) {

    async.waterfall([
      function (callback) {
        getScores(query.id)
          .then(result => {
            let res = result.data.data;
            if (_.isUndefined(res) || _.isEmpty(res))
              callback('Survey scores not found');
            else
              callback(null, res);
          })
          .catch(err => callback(err));
      },
      function (surveyScores, callback) {
        data = scoresFunction(surveyScores);
        callback(null, data);
      },
      function (data, callback) {
        SurveyService.updateSurvey(data, query.id)
          .then(updated => {
            callback(null, updated);
          })
          .catch(err => callback(err));
      }
    ], function (err, result) {
      if (err)
        return reject(new Error(err));
      else {
        SurveyService.getSurveyData(query.id)
          .then(res => {
            resolve(res)
          })
          .catch(reject);
      }
    });
  });
}

// get survey scores
function scoresFunction(scores) {

  let scoreData = _.mapValues(_.groupBy(scores, 'section'));
  let scoreSection = {};
  let formsData = [];

  _.forEach(scoreData, (val, key) => {
    scoreData[key] = _.mapValues(_.groupBy(val, 'category'))
  })

  _.forEach(scoreData, (value, key) => {
    scoreSection[key] = {};
    _.forEach(value, (res, item) => {
      scoreSection[key][item] = _.sumBy(res, function (o) { return o.weight });
    })
  })

  scoreSection.overall = sumObjectsByKey(scoreSection)

  return scoreSection;
}

// sum objects
function sumObjectsByKey(obj) {

  let data = []

  _.forEach(obj, (val, key) => {
    data.push(val)
  })

  var obj = {}

  if (data.length > 1) {
    Object.keys(data[0]).map(function (val) {
      obj[val] = data[0][val] + data[1][val]
    })
  }
  else
    obj = data[0];

  return obj;
}

function surveyResults(requestData) {

  var query = Cobuild.Utils.Request.formatQueryString(requestData.params);
  var company = '';

  return new Promise(function (resolve, reject) {

    let req;
    SurveyService.getSurveyScore(query.id)
      .then(function (result) {
        result = result.data.data;

        if(!_.isEmpty(result)){
          company = !_.first(result) ? '' : _.first(result).company;
          result = _.map(result, (survey)=>{
            delete survey.company;
            return survey;
          })
        }

        compareResults(result)
          .then(res => {
            data = res;
            data[0].survey = query.id;
            let coreData = getScore(requestData).then(res => {
              if (_.isUndefined(res) || _.isEmpty(res))
                req = create(data[0])
              else
                req = update(res.id, data[0])

              req.then(res => {
                res.company = company;
                resolve(res);
              })
            });

          })
          .catch(reject);
      })
      .catch(reject);

  });
}

const getTotalQuestions = (form) => {
  const questions = form.subsections[0].questions;
  const count = _.sumBy(questions, (val) => val.question.score ? 1 : 0)
  return count;
}

const calculateSurveyRisk = ({cyberrisk, categories, form}) => {
  const { scores } = config;
  let scoreLevel = 'low';
  let chartLimits = {};

  const totalQuestions = getTotalQuestions(form[0]);
  const highestScore = totalQuestions * scores.low;
  const lowestScore = totalQuestions * scores.high;
  const limitPercent = {
    low: (highestScore - (totalQuestions * scores.low)) / 3,
    medium: (highestScore - (totalQuestions * scores.medium)) / 3,
    high: (highestScore - (totalQuestions * scores.high)) / 3,
  };
  const limit = Math.ceil(limitPercent.high / 10) * 10;
  let count = 0;

  _.forEach(scores, (val, key) => {
    if(count > 0)
      count += limit
    else
      count = lowestScore + limit;
    
    chartLimits[key] = count;
  })

  chartLimitsSorted = Object.keys(chartLimits).sort(function(a,b){return chartLimits[a]+chartLimits[b]})
  _.forEach(chartLimitsSorted, (val) => {
    if(cyberrisk.score <= chartLimits[val]){
      scoreLevel = val;
    }
  })

  return {
    risk: {
      score: cyberrisk.score,
      level: scoreLevel
    },
    company: categories.company,
    categories: categories.categories,
    surveyType: categories.surveyType,
    chartLimits: chartLimits,
    questionsScores: {
      low: totalQuestions * scores.low,
      medium: totalQuestions * scores.medium,
      high: totalQuestions * scores.high
    }
  }
}


const results = (requestData) => {

  var query = Cobuild.Utils.Request.formatQueryString(requestData.params);

  return new Promise(function (resolve, reject) {
    const getSurveyRisk = (cb) => {
      SurveyService.getSurveyRiskScore(query.id)
        .then(function (res) {
          if (_.isUndefined(res) || _.isEmpty(res))
            cb('Could not get the cyberscore');
          else
            return cb(null, res)
        })
        .catch(err => cb(err));
    }

    const getCategories = (cbData, cb) =>{
      SurveyService.getCategoriesLevels(query.id)
        .then(function (res) {
          if (_.isUndefined(res) || _.isEmpty(res))
            cb('Could not get the categories');
          else {
            const score = {
              cyberrisk: cbData,
              categories: res
            }
            return cb(null, score)
          }
        })
        .catch(err => cb(err));
    }

    const getForm = (cbData, cb) =>{
      const type = cbData.categories.surveyType;
      SurveyService.getForm(type)
        .then(function (res) {
          if (_.isUndefined(res) || _.isEmpty(res))
            cb('Could not get the form');
          else {
            cbData.form = res;
            return cb(null, cbData)
          }
        })
        .catch(err => cb(err));
    }

    var tasks = [
      getSurveyRisk,
      getCategories,
      getForm,
    ]

    async.waterfall(tasks, (err, response)=>{
      if(err) return reject(err);

      const { cyberrisk, categories, form } = response;
      const result = calculateSurveyRisk({ cyberrisk, categories, form })
      resolve(result)
    })
  });
}

function compareResults(data) {

  let threshold = {
    results: data
  };

  return new Promise(function (resolve, reject) {
    async.parallel({
      basic: (cb) => {
        calculateRisk({ form: 'basic' }).then(function (basic) {
          if (_.isUndefined(basic) || _.isEmpty(basic))
            cb('Could not load the form');
          else
            return cb(null, basic)
        })
          .catch(err => cb(err));
      },
      advanced: (cb) => {
        calculateRisk({ form: 'advanced' }).then(function (advanced) {
          if (_.isUndefined(advanced) || _.isEmpty(advanced))
            cb('Could not load the form');
          else
            cb(null, advanced);
        })
          .catch(err => cb(err));
      },
      overall: (cb) => {
        calculateOverallScore().then(function (overall) {
          if (_.isUndefined(overall) || _.isEmpty(overall))
            cb('Could not load the form');
          else
            cb(null, overall);
        })
          .catch(err => cb(err));
      },
    },
      (err, dataForm) => {
        if (err)
          return reject(err)

        threshold.basic = dataForm.basic;
        threshold.advanced = dataForm.advanced;
        threshold.overall = dataForm.overall;

        data = _.head(data);

        _.forEach(data.scores, (value, key) => {
          let values = {};
          values = _.map(value, (item, index) => {
            let result = {};
            result[index] = {
              value: item
            };
            _.forEach(threshold[key][index], (items, i) => {
              if (item <= items && !result[index].level)
                result[index].level = i;
            });
            return result;
          });
          data.scores[key] = values
        });

        let dataScore = {};
        if (data.scores.advanced)
          dataScore = _.values(threshold.overall);
        else
          dataScore = _.values(threshold.basic);

        let totalScore = _.sumBy(data.scores.overall, (data) => {
          return data[Object.keys(data)].value;
        });
        let totalLevels = {};
        _.forEach(types, (type) => {
          totalLevels[type] = _.sumBy(dataScore, type);
        });
        threshold.results[0].scores.total = {
          level: totalLevels,
          risk: totalScore
        };
        resolve(threshold.results)
      });
  });
}

function getScore(req) {
  return new Promise(function (resolve, reject) {
    Risk
      .from('risk')
      .where({ survey: req.params.id })
      .first()
      .then(result => {
        resolve(result);
      })
      .catch(reject)
  })
}

// create risk results
function create(data) {
  return new Promise(function (resolve, reject) {
    Risk('risk')
      .insert(data, '*')
      .then(function (result) {
        resolve(result.shift());
      })
      .catch(reject);
  })
};

// update risk results
function update(id, data) {
  return Risk('risk')
    .update(data)
    .where({ id: id })
    .then(function (updated) {
      return Risk
        .from('risk')
        .where({ id: id })
        .first()

    });
};

function getScores(id) {
  let req = {
    url: `http://survey:1337/api/v1/surveys/${id}/scores`,
    json: true,
    headers: {
      'Authorization':'Bearer ' + config.iam.adminToken
    }
  }

  return new Promise((resolve, reject) => {
    request(req, (err, response, body) => {
      if (!err)
        return resolve({ data: body });
      return reject(new Error('Error requesting scores'));
    })
  })
};

function monetaryRisk(req){
  return new Promise((resolve, reject) => {
    let id = req.params.id || req.query.id;

    const getSurveyFn = (cb)=>{
      SurveyService.getSurveyData(id)
      .then((response)=>{
        cb(null, response.data);
      })
      .catch(cb)
    }

    const getScoresFn = (cb)=>{
      surveyResults(req)
      .then((response)=>{
        cb(null, response.scores);
      })
      .catch(cb)
    }
    

    function generalFormat(payload, survey){
      const required = [{
        'name': 'basic.customers',
        'request': 'customers'
      }, 
      {
        'name': 'basic.employees',
        'request': 'employees'
      },
      {
        'name': 'basic.revenue',
        'request': 'yearly_revenue'
      },
      {
        'name': 'basic.partners',
        'request': 'it_suppliers'
      }]

      required.forEach((row)=>{
        payload[row.request] = parseInt(survey.questions[row.name]) || 0
      })

      return payload;
    }

    function sensitiveFormat(payload, sensitive){
      sensitive     = sensitive.split(',');

      sensitive.forEach((row)=>{
        payload['sensitive'][row] = true;
      })

      return payload;
    }


    function groupFormat(payload, scores){
      const groups = {
        'data_breach': ['Data Breach'],
        'business_interruption': ['DDoS', 'S/W Vuln.'],
        'e_theft': ['Spear-Phishing']
      }

      const levels = {
        'L': 1,
        'M': 2,
        'H': 3
      }
      

      for (var name in groups) {
        for (var i = 0; i < groups[name].length; i++) {
          var category = groups[name][i];
          var score = _.find(scores, (value)=>{
            return _.keys(value)[0] == category;
          });
          score = !score[category] ? 'L' : score[category].level;
          var value = levels[score];
          var currentScore = !payload['level'][name] ? 'L' : payload['level'][name];
          var currentValue = levels[currentScore];
          payload['level'][name] = (value >= currentValue) ? score : currentScore
        }
      }

      return payload;
    }

    const getMonetaryFn = ['survey', 'scores', (cb, cbData)=>{
      let payload = {
        level: {},
        sensitive: {}
      };

      const survey = cbData.survey.data;
      const scores = cbData.scores.basic;


      if(!survey.questions) 
        return cb(null, {})


      payload = generalFormat(payload, survey) || {}
      
      if(!survey.questions['basic.sensitive'])
        return cb(null, {});

      let sensitive = survey.questions['basic.sensitive'];
      payload = sensitiveFormat(payload, sensitive) || {}
      payload = groupFormat(payload, scores) || {};


      CalculatorService.getMonetaryRisk(payload)
      .then((response)=>{
        cb(null, response.data.data);
      })
      .catch(cb);

    }]


    const getSummaryFn = ['survey', 'scores', 'monetary', (cb, cbData)=>{
      const scores = cbData.scores.basic;
      const survey = cbData.survey.data;
      const monetary = cbData.monetary;

      if(!monetary.summarize || !survey.questions) 
        return cb(null, []);

      const summarize = monetary.summarize;
      const questions = survey.questions;

      const payload = {
        'avgRisk': summarize.avgRisk,
        'totalRisk': summarize.totalRisk,
        'yearlyRevenue': parseInt(questions['basic.revenue ']),
        'categories': scores,
        'suppliers': questions['basic.partners']
      }
      
      CalculatorService.getSummaryRisk(payload)
      .then((response)=>{
        cb(null, response.data.data);
      })
      .catch(cb);
    }]

    const tasks = {
      survey: getSurveyFn,
      scores: getScoresFn,
      monetary: getMonetaryFn,
      summary: getSummaryFn
    }

    async.auto(tasks, (err, response)=>{
      if(err) return reject(err);

      delete response.survey;
      delete response.scores;
      resolve(response)
    })
  });
}

module.exports = {
  results,
  getScore,
  monetaryRisk,
  surveyResults,
  calculateRisk,
  calculateScoresValue,
  calculateOverallScore
};