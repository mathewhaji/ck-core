var Cobuild           = require('cobuild2');
var config           = Cobuild.config;
var express           = require('express');
var router            = express.Router();
var userController   = require(Cobuild.Utils.Files.dirpath(__dirname)+'/controllers/userController');
var userValidator    = require(Cobuild.Utils.Files.dirpath(__dirname)+'/validators/user');



module.exports = function(app, limiter) {

  /**
  *   Limiter
  *     For more info see: https://www.npmjs.com/package/express-limiter
  */

  if (limiter){
    // Here insert the rules for use limiter.
    // Example
    limiter({
      path: '/api/'+Cobuild.config.apiVersion+'/users/health',
      method: 'get',
      lookup: ['connection.remoteAddress'],
      // 10 requests per 10 seconds
      total: 10,
      expire: 1000 * 10 ,
      onRateLimited: function (req, res, next) {
        next({ message: 'Rate limit exceeded', status: 400 })
      }
    });
  } 

  /**
  * The Policies load when init a server, in that policies are some validations like:
  * isLoggedIn or isAdmin
  * To use:
  * 
  * var authPolicyIsLoggedIn      = Cobuild.Policies.getPolicy('cobuild.'+config.authentication.strategy+'.isLoggedIn');
  * var authPolicyIsAdmin      = Cobuild.Policies.getPolicy('cobuild.'+config.authentication.strategy+'.isAdmin');
  * 
  * On the declaretion of end-point call a policy
  *
  * router.put('/', authPolicyIsLoggedIn, Ctrl.update);
  *
  */
  
	router.get('/health', function(req, res) {
    
     return res.status(200).send('ok');
  });

/**
 * @api {post} /api/v1/users Create a User
 * @apiName User - Create
 * @apiGroup User
 * @apiDescription Create a User into the database
 * @apiVersion 1.0.0
 *
 * @apiParam {String} title A random title param of this record
 *
 * @apiParamExample {json} Request-Example:
 * {
 *   "title": "Hello world"
 * }
 *
 * @apiSuccessExample {JSON} response-example:
 * {
 *   "__v": 0,
 *   "title": "Hello world",
 *   "_id": "573f2904e383af8c2e612b5e"
 * }
 */  
  router.post('/', userController.create);

  router.post('/activate/:uid', userController.activateUser);
/**
 * @api {post} /api/v1/assign-password/:uid Assign password
 * @apiName User - Assign password
 * @apiGroup User
 * @apiDescription Assign password
 * @apiVersion 1.0.0
 *
 * @apiParam {String} uid user uid
 *
 *
 * @apiSuccessExample {JSON} response-example:
 * {
 *   "__v": 0,
 *   "firstName": "Deo",
 *   "lastName": "Rokk3rlabs",
 *   "email": "doe@rokk3rlabs.com",
 *   "id": "215"
 * }
 */
  router.post('/assign-password/:uid', userController.assignPassword);

/**
 * @api {post} /api/v1/assign-user/:uid Get information about the user that was assigned
 * @apiName User - Get information about the user that was assigned
 * @apiGroup User
 * @apiDescription Get information about the user that was assigned
 * @apiVersion 1.0.0
 *
 * @apiParam {String} uid user uid
 *
 *
 * @apiSuccessExample {JSON} response-example:
 * {
 *   "__v": 0,
 *   "firstName": "Deo",
 *   "lastName": "Rokk3rlabs",
 *   "email": "doe@rokk3rlabs.com",
 *   "id": "215"
 * }
 */
  router.post('/assign-user/:uid', userController.assignedUser);

/**
 * @api {post} /api/v1/users-company/:id Create a company user
 * @apiName User - Create a company user
 * @apiGroup User
 * @apiDescription Create a company user
 * @apiVersion 1.0.0
 *
 * @apiParam {String} id company id
 *
 * @apiParamExample {json} Request-Example:
 * {
 *   "companyName": "Rokk3rlabs",
 *   "firstName": "Doe",
 *   "lastName": "Rokk3r",
 *   "email": "doe@rokk3rlabs.com"
 * }
 * @apiSuccessExample {JSON} response-example:
 * {
 *   "id": "45454",
 *   "firstName": "Doe",
 *   "lastName": "Rokk3r",
 *   "email": "doe@rokk3rlabs.com"
 * }
 */
  router.put('/users-company/:id', userController.createUserCompany);

  router.post('/status', userController.statusUser);
/**
 * @api {get} /api/v1/users-company/:id Get user's company
 * @apiName User - Get user's company
 * @apiGroup User
 * @apiDescription Get information about the user that was assigned
 * @apiVersion 1.0.0
 *
 * @apiParam {String} id user id
 *
 *
 * @apiSuccessExample {JSON} response-example:
 * {
 *   "id": "45454"
 * }
 */
  router.get('/company/id', userController.getUserCompanyId);
/**
 * @api {get} /api/v1/company/all/:companyId Get all users of a company
 * @apiName User -  Get all users of a company
 * @apiGroup User
 * @apiDescription  Get all users of a company
 * @apiVersion 1.0.0
 *
 * @apiParam {String} id company id
 *
 *
 * @apiSuccessExample {JSON} response-example:[{
 *   "__v": 0,
 *   "firstName": "Deo",
 *   "lastName": "Rokk3rlabs",
 *   "email": "doe@rokk3rlabs.com",
 *   "id": "215"
 * }]
 */
  router.get('/company/all/:companyId', userController.getCompanyUsers)

  router.get('/reports/user', userController.getReportsByUser)

  /**
   * @api {post} /api/v1/users/forgot-password Forgot password
   * @apiName forgot_password
   * @apiGroup User
   * @apiDescription Makes a request to restore password, sends an email with numerical code to authorize the change
   * @apiVersion 1.0.0
   *
   * @apiParam {string} email user's email.
   *
   * @apiParamExample {json} Request-Example:
   * {
   *   "email": "test@test.com",
   * }
   *
   * @apiSuccessExample {JSON} response-example:
   *  {
   *    "data": {
   *      "msg": "Code has been sent to your email"
   *    }
   *  }
   *
   */

  router.post('/forgot-password', userController.forgotPassword);

  /**
   * @api {post} /api/v1/users/reset-password Reset password
   * @apiName reset_password
   * @apiGroup User
   * @apiDescription Makes a request to restore password, sends an email with numerical code to authorize the change
   * @apiVersion 1.0.0
   *
   * @apiParam {string} email user's email.
   *
   * @apiParamExample {json} Request-Example:
   * {
   *   "email": "test@test.com",
   * }
   *
   * @apiSuccessExample {JSON} response-example:
   *  {
   *    "data": {
   *      "message":"changed_password"
   *    }
   *  }
   *
   */

  router.post('/reset-password', userController.resetPassword);

  /**
 * @api {post} /api/v1/users/reset-password Reset password
 * @apiName resetPassword
 * @apiGroup User
 * @apiDescription Create a new password
 * @apiVersion 1.0.0
 *
 * @apiParam {string} code confirmation code
 * @apiParam {string} email user's email
 * @apiParam {string} password new password
 *
 * @apiSuccessExample {JSON} response-example:
 *  {
 *    "data":
 *    {
 *      "gender": "Male",
 *      "email": "me@rokk3rlabs.com",
 *      "lastName": "Torvalds",
 *      "firstName": "Linus",
 *      "password": "$2a$10$NoJV5izPvB1HK2v./MgxC.x/8/wGUGmCf0SpIHBiPDdAHSyyzOwZa",
 *      "_id": "56a7c125706144ab0b2ab7e9",
 *      "updatedAt": "2016-01-26T18:55:33.800Z",
 *      "createdAt": "2016-01-26T18:55:33.800Z",
 *      "image": "http://simpleicon.com/wp-content/uploads/user1.png",
 *      "isActive": true,
 *      "role": "User"
 *    }
 * }
 *
 * @apiError {Object} error error object
 * @apiError (error) {Int} code server error code
 * @apiError (error) {String} userMessage friendly message to show 
 * @apiError (error) {String} serverInfo server information to know where the error come from
 * @apiError (error) {Object} data extra data to show or use if neccesary 
 *
 * @apiErrorExample {JSON}  Error-Example
 *
 *  {
 *     error:
 *     {
 *         "code": 401
 *         "userMessage": "Friendly Error String",
 *         "serverInfo": "model_controller_method_extraInfo",
 *         "data": {}
 *     } 
 *  }
 */

  router.post('/reset-password/:token', userController.resetPassword);

/**
 * @api {get} /api/v1/users Read records for a User
 * @apiName User - Read
 * @apiGroup User
 * @apiDescription Read several User given conditions
 * @apiVersion 1.0.0
 * 
 *
 * @apiParamExample {json} Request-Example:
 * {
 * }
 * 
 * @apiSuccessExample {JSON} response-example:* 
 * [
 *   {
 *     "_id": "573f5e2bd661390c453a64d8",
 *     "title": "Hello world",
 *     "__v": 0
 *   },
 *   {
 *     "_id": "573f5e2cd661390c453a64d9",
 *     "title": "Hello world",
 *     "__v": 0
 *   },
 *   {
 *     "_id": "573f5e33d661390c453a64da",
 *     "title": "Hello world 2",
 *     "__v": 0
 *   },
 *   {
 *     "_id": "573f5e36d661390c453a64db",
 *     "title": "Hello world 3",
 *     "__v": 0
 *   },
 *   {
 *     "_id": "573f5e3ad661390c453a64dc",
 *     "title": "Hello world 4",
 *     "__v": 0
 *   }
 * ]
 */

  router.get('/', userController.list);
/**
 * @api {get} /api/v1/payload/policies Get policies for a User
 * @apiName User - Get policies
 * @apiGroup User
 * @apiDescription Get policies for a User
 * @apiVersion 1.0.0
 * 
 *
 * @apiParamExample {json} Request-Example:
 * {
 * }
 * 
 * @apiSuccessExample {JSON} response-example:* 
 * [
 *   [*]
 * ]
 */
  router.get('/payload/policies', userController.getPolicies)

  /**
 * @api {get} /api/v1/users/cursor  Read :limit records up to :max id or :since id
 * @apiName User - Cursor
 * @apiGroup User
 * @apiDescription Read :limit User given :max or :since ids
 * @apiVersion 1.0.0
 *
 * @apiParam {string} max id of the User
 * @apiParam {string} since id of the User
 * @apiParam {string} limit desired number  of the User instances
 *
 * @apiParamExample {json} Request-Example:
 * {
 * }
 * 
* @apiSuccessExample {JSON} response-example:* 
 * [
 *   {
 *     "_id": "573f5e2bd661390c453a64d8",
 *     "title": "Hello world",
 *     "__v": 0
 *   },
 *   {
 *     "_id": "573f5e2cd661390c453a64d9", 
 *     "title": "Hello world",
 *     "__v": 0
 *   },
 *   {
 *     "_id": "573f5e33d661390c453a64da",
 *     "title": "Hello world 2",
 *     "__v": 0
 *   },
 *   {
 *     "_id": "573f5e36d661390c453a64db",
 *     "title": "Hello world 3",
 *     "__v": 0
 *   },
 *   {
 *     "_id": "573f5e3ad661390c453a64dc",
 *     "title": "Hello world 4",
 *     "__v": 0
 *   }
 * ]
 */

  router.get('/cursor', userController.cursor);

  /**
 * @api {get} /api/v1/users/me  Get all details about user
 * @apiName User - Get account
 * @apiGroup User
 * @apiDescription Get user information
 * @apiVersion 1.0.0
 *
 * 
 * @apiSuccessExample {JSON} response-example:* 
 
 *   {
 *     "id": "573f5e2bd661390c453a64d8",
 *     "name": "Rokk3rlabs",
 *     "email": "test@rokk3rlabs",
 *     "__v": 0
 *   }
 */
router.get('/me', userController.getAccount);
  

  /**
 * @api {get} /api/v1/users/:id  Read a record for a User with given id
 * @apiName User - Read
 * @apiGroup User
 * @apiDescription Read one User given id
 * @apiVersion 1.0.0
 *
 * @apiParam {string} id of the User
 *
 * @apiParamExample {json} Request-Example:
 * {
 * }
 * 
 * @apiSuccessExample {JSON} response-example:* 
 
 *   {
 *     "_id": "573f5e2bd661390c453a64d8",
 *     "title": "Hello world",
 *     "__v": 0
 *   }
 */
  router.get('/:id', userController.list);

/**
 * @api {put} /api/v1/users Update a User
 * @apiName User - Update
 * @apiGroup User
 * @apiDescription Update a User given their id
 * @apiVersion 1.0.0
 *
 * @apiParam {string} id of the User
 *
 * @apiParamExample {json} Request-Example:
 * {
 *   "title": "Hello world 2",
 *   "id": "573f22b74db4608025e7abf4"
 * }
 *
 * @apiSuccessExample {JSON} response-example:
 * {
 *   "__v": 0,
 *   "title": "Hello world 2",
 *   "_id": "573f22b74db4608025e7abf4"
 * }
 */ 
  router.put('/', userValidator.haveId, userController.update);
  router.put('/:id', userValidator.haveId, userController.update);


  router.put('/role/:role', userValidator.haveId, userController.changeRole);

/**
 * @api {put} /api/v1/users/merge/accounts Merge accounts
 * @apiName User - Merge accounts
 * @apiGroup User
 * @apiDescription Merge accounts
 * @apiVersion 1.0.0
 *
 * @apiParamExample {json} Request-Example:
 * {
 *   "guestEmail": "guest@rokk3rlabs.com",
 *   "email": "user@rokk3rlabs.com",
 *   "password": "123456"
 * }
 *
 * @apiSuccessExample {JSON} response-example:
 * {
 *   "__v": 0,
 *   "title": "Hello world 2",
 *   "_id": "573f22b74db4608025e7abf4"
 * }
 */ 
  router.put('/merge/accounts', userController.mergeAccounts)

/**
 * @api {delete} /api/v1/users Delete a User instance
 * @apiName User - Delete
 * @apiGroup User
 * @apiDescription Delte a User given their id
 * @apiVersion 1.0.0
 *
 * @apiParam {string} id of the User to be deleted
 *
 * @apiParamExample {json} Request-Example:
 * {
 *   "id": "573f22b74db4608025e7abf4"
 * }
 *
 * @apiSuccessExample {JSON} response-example:
 * {
 *   "_id": "573f22b74db4608025e7abf4"
 * }
 */ 
  router.delete('/', userValidator.haveId,userController.destroy);
  router.delete('/:id', userValidator.haveId, userController.destroy);


  console.log('/api/'+Cobuild.config.apiVersion+'/users');
  app.use('/api/'+Cobuild.config.apiVersion+'/users',router);

  

}