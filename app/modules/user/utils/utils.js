var moment  = require('moment');
var Hashids = require('hashids');

var exports = module.exports;

exports.generateEmail = function generateEmail() {
  return "guest+"+moment().unix()+"@cytrust.com";
}

exports.generatePassword = function generatePassword(email){
  var hashids = new Hashids(email);
  return hashids.encode(458,681,254,739)
}