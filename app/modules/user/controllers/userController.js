var Cobuild                     = require('cobuild2');
var _                           = require('lodash');
var userService    = require(Cobuild.Utils.Files.dirpath(__dirname)+'/services/userService');

var exports                     = module.exports;

exports.create = function(req, res) {
  
  userService.create(req.body)
  .then(function(record) {
    res.ok(record)
  })
  .catch(function(err){
    res.badRequest(err);
  });

};

exports.list = function(req, res) {
  
  var id = req.params.id || req.query.id;

  userService
  .list(req)
  .then(
    
    function success(result){

      if(id !== undefined){
        res.ok((result && result.data.length > 0)? result.data[0] : {});
      }else{
        res.ok(result);  
      }
    },

    function error(err){
      res.badRequest(err);
    });
  

};

exports.getAccount = function(req, res) {

  let id = _.get(res, 'locals.identity.uid');
  
  userService
  .getAccount(id)
  .then(function (result) {
    res.ok(result)
  })
  .catch(function (err) {
    if (err.code < 500) {
      res.badRequest(err.error);
    }
    else {
      res.serverError(err.error);
    }
  });
  
};

exports.cursor = function(req, res){



  var id = req.params.id || req.query.id;
  console.log('cursor:',id);

  userService
  .cursor(req)
  .then(
    
    function success(result){
      if(id !== undefined){
        res.ok((result && result.data.length > 0)? result.data[0] : {});
      }else{
        res.ok(result);  
      }
    },

    function error(err){
      res.badRequest(err);
    });
};

exports.forgotPassword = function (req, res) {

  userService.forgotPassword(req.body, req.headers['user-agent'])
    .then(function (result) {
      res.ok(result)
    })
    .catch(function (err) {
      if (err.code < 500) {
        res.badRequest(err.error);
      }
      else {
        res.serverError(err.error);
      }
    });

};

exports.resetPassword = function (req, res) {
  
  userService.resetPassword(req.params, req.body)
    .then(function (result) {
      res.ok(result)
    })  
    .catch(function (err) {
      if (err.code < 500) {
        res.badRequest(err.error);
      }
      else {
        res.serverError(err.error);
      }
    });

};

exports.update = function(req, res){

  var id = req.body.id || req.body._id  || req.params.id;

  //console.log("id to update:", id)
  var data = _.clone(req.body);
  delete data.id;

  userService
  .update(id, data)
  .then(function (record) {
    res.ok(record);
  })
  .catch(function(err){
    res.badRequest(err);
  });
  
  

};

exports.destroy = function(req, res) {
  
  console.log("req.body:", req.body);
  console.log("req.params:", req.params);

  var id = req.body.id || req.params.id;
  console.log("delete id:", id);

  userService
  .delete(id)
  .then(function (record) {
    res.ok(record);
  })
  .catch(function(err){
    res.badRequest(err);
  });

};


exports.getPolicies = function(req, res){
  var uid = _.get(res, 'locals.identity.uid');

  userService
  .getPolicies(uid)
  .then(function (record) {
    res.ok(record);
  })
  .catch(function(err){
    res.badRequest(err);
  });
}


exports.activateUser = function(req, res){
  var uid = req.params.uid; 
  userService
  .activateUser(uid)
  .then(function(record){
    res.ok(record)
  })
  .catch(function(err){
    res.badRequest(err); 
  })
}

exports.assignPassword = function(req, res){
  var uid = req.params.uid; 
  userService
  .assignPassword(uid, req.body)
  .then(function(record){
    res.ok(record)
  })
  .catch(function(err){
    res.badRequest(err); 
  })
}

exports.statusUser = function(req, res){
  var email = req.body.email;

  userService
  .statusUser(email)
  .then(function(record){
    res.ok(record);
  })
  .catch(function(err){
    res.badRequest(err);
  })
}


exports.mergeAccounts = function(req, res){
  var data = req.body;
  var uid = _.get(res, 'locals.identity.uid');
  userService
  .mergeAccounts(data, uid)
  .then(function(record){
    res.ok(record);
  })
  .catch(function(err){
    res.badRequest(err);
  })
};

exports.assignedUser = function(req, res){
  var uid = req.params.uid; 
  userService
  .assignedUser(uid)
  .then(function(record){
    res.ok(record);
  })
  .catch(function(err){
    res.badRequest(err);
  })

}

exports.createUserCompany = function(req, res){
  var fromUser = _.get(res, 'locals.identity.uid');
  userService
  .createUserCompany(req.params.id, req.body, fromUser)
  .then(function(record){
    res.ok(record);
  })
  .catch(function(err){
    res.badRequest(err);
  })
}

exports.getUserCompanyId = function(req, res){
  var uid = _.get(res, 'locals.identity.uid');
  userService
  .getUserCompanyId(uid)
  .then(function(record){
    res.ok(record);
  })
  .catch(function(err){
    res.badRequest(err);
  })
}

exports.getCompanyUsers = function(req, res){
  userService
  .getCompanyUsers(req)
  .then(function(record){
    res.ok(record);
  })
  .catch(function(err){
    res.badRequest(err);
  })
};


exports.changeRole = function(req, res){

  userService
  .changeRole(req.params.role, req.body)
  .then(function(record){
    res.ok(record)
  })
  .catch(function(err){
    res.badRequest(err);
  })
}

exports.getReportsByUser = function(req, res){
  var uid = _.get(res, 'locals.identity.uid');
  userService
  .getReportsByUser(req, uid)
  .then((response)=>{
    res.ok(response)
  })
  .catch((err)=>{
    res.badRequest(err);
  })
}

