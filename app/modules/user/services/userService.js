var Cobuild           = require('cobuild2');
var _                 = require('lodash');
var async             = require('async');
var config            = Cobuild.config; 
var User              = require(Cobuild.Utils.Files.dirpath(__dirname)+'/models/user');
var UserCompany       = require(Cobuild.Utils.Files.dirpath(__dirname)+'/models/userCompany');
var Report            = Cobuild.Utils.Files.requireIfExists('/app/modules/reports/models/report');
var config            = Cobuild.config;
var request           = require('request-promise');
var debug             = require('debug')('userService');
var bcrypt            = require('bcrypt-nodejs');
var utils             = require(Cobuild.Utils.Files.dirpath(__dirname)+'/utils/utils');
var i18n              = require('i18n');
var mailService       = require(Cobuild.Utils.Files.dirpath(__dirname)+'/services/mailService');
var exports           = module.exports;



exports.create = function(data){

  return new Promise(function(resolve, reject){
      let guest = false;
      let achievedTasks = {};
      let role = data.role ? data.role : 'default'; 
      delete data.role;

      let policies = {
        'admin': ['srn:iam:default:policies:company-owner'],
        'user': ['srn:iam:default:policies:company-user'],
        'owner': ['srn:iam:default:policies:company-owner'],
        'default': ['srn:iam:default:policies:user']
      }

      if(!data.email){
        let email = utils.generateEmail();
        data.email = email;
        guest = true;
      }

      if(!data.password){
        let password = utils.generatePassword(data.email);
        data.password = password;
        guest = true;
      }

      let createAccount = (cb) => {
        let params              = _.pick(data, ['email','password']);
        params.client_id        = Cobuild.config.authServer.clientId;
        params.client_secret    = Cobuild.config.authServer.clientSecret;

        request({
            url:'http://auth:1337/api/v1/register',
            method:'POST',
            body: params,
            json:true
        })
        .then(function(response){
          achievedTasks.createAccount = response;
          cb(null, response)
        })
        .catch(cb);

      };

      let createIdentity = ['account', (cb, prevData) => {
   
        debug('prevData:', prevData);
        debug('Cobuild.config.iam.adminToken:', Cobuild.config.iam.adminToken);


        let params = {
            policies: policies[role],
            email: data.email,
            cob_auth: prevData.account._id,
            client_id: Cobuild.config.authServer.clientId,
            client_secret: Cobuild.config.authServer.clientSecret
        };

        let options = {
            url:'http://iam:1337/api/v1/identities',
            method:'POST',
            body:params,
            json:true,
            headers:{
                'Authorization':'Bearer ' + Cobuild.config.iam.adminToken
            }
        };

        request(options)
        .then(body => {
            debug("add agent iam response",body);
            try {
                var response = body.data
                response.uid = response._id;
                achievedTasks.createIdentity= response;

                cb(null,response);
            }catch(exception){
                cb(exception);
            }

        })
        .catch(cb)
      }];

      let createUser = ['identity', (cb, prevData) => {
        
        debug('prevData:', prevData);

        let payload = Object.assign({}, data);

        payload.uid = prevData.identity.uid;
        payload.active = guest ? true : false;
        delete payload.password;

        User('user')
        .insert(payload,'*')
        .then(function(result) {
          
          let user = result.shift()
          debug(user);
          achievedTasks.createUser = user;
          cb(null, user);
        })
        .catch(cb);
      }];


      let sendMail = ['user', (cb, prevData) => {
        let user   = prevData.user; 
        
        mailService.sendActivation(user)
        .then(response=>cb(null, response))
        .catch(cb);
      }]

      let login = ['user', (cb, prevData)=>{
        let params  = _.pick(data, ['email','password']);
        authLogin(params, cb)
      }]

      let tasks = {
        account: createAccount,
        identity: createIdentity,
        user: createUser
      };

      if(!guest)
        tasks.mail = sendMail;
      else
        tasks.login = login;

      async.auto(tasks, (err, result) => {
        if(err){
          debug('rollbackUserCreation:', achievedTasks);
          return exports.rollbackUserCreation(achievedTasks)
          .then(result => {
            debug('rollbackUserCreation result:', result);
            reject(err);
          })
          .catch(error => {
            console.error('rollbackUserCreation error:', error);
            reject([err, error]);
          });
        }

        return (!guest) ? resolve(result.user) : resolve({login: result.login, user: result.user});

      });

      
  })
};


exports.mergeAccounts = async (data, uid) => {
  try {
    let userFound = await findByEmail(data.email);
  
    if(!_.isEmpty(userFound)) 
      return Promise.reject(i18n.__('email_already_exists'));
  
    let params   = _.pick(data, ['email','password']);
    params.password         = bcrypt.hashSync(params.password);
    params.client_id        = Cobuild.config.authServer.clientId;
    params.client_secret    = Cobuild.config.authServer.clientSecret;

    let authResponse = await request({
        url:'http://auth:1337/api/v1/user',
        method:'PUT',
        body: params,
        json:true,
        headers:{
          'Authorization':'Bearer ' + data.guestToken
        }
    });

    console.log('===== authResponse ===== ', authResponse);

    let iamParams   = _.pick(data, ['email']);

    let iamResponse = request({
      url:'http://iam:1337/api/v1/identities/' + uid,
      method:'PUT',
      body: iamParams,
      json:true,
      headers:{
        'Authorization':'Bearer ' + Cobuild.config.iam.adminToken
      }
    });
  
    console.log('===== iamResponse ===== ', iamResponse);
    
    let userUpdate = await User('user')
                      .update({email: data.email, active: false})
                      .where({email:data.guestEmail});

    let user = await findByEmail(data.email);

    let sendMail = mailService.sendActivation(user);
  
    return user;
    
  } catch (error) {
    console.log('===== err ===== ', error);
  }
};

exports.createUserCompany = (companyId, user, fromUser)=>{
  return new Promise((resolve, reject) => {
    var role = user.role; 

    var createUser = (cb)=>{
      exports.create(user)
      .then((response)=>{
        cb(null, response)
      })
      .catch(cb)
    }

    var assignUserCompany = (cbData, cb)=>{
      var user = cbData.user;
      var payload = {
        user: user.id,
        company: companyId,
        role: role
      }

      UserCompany('user_company')
      .insert(payload,'*')
      .then(function(result) {
        cb(null, user);
      })
      .catch(cb);
    };

    var sendMail = (cbData, cb)=>{
      let user   = cbData; 
      mailService.sendAssignedUser(user, companyId, fromUser)
      .then(response=>cb(null, user))
      .catch(cb);
    }

    var tasks = [
      createUser,
      assignUserCompany
    ]

    if(!user.password)
      tasks.push(sendMail);


    async.waterfall(tasks, (err, result)=>{
      if(err) return reject(err);

      resolve(result)
    })
  });

}

exports.rollbackUserCreation = (achievedTasks) => {
  return new Promise((resolve, reject) => {
    let tasks = [];

    if(achievedTasks.createAccount){
    
      let deleteAccount = (cb) => {


        
        let params = {
          id: achievedTasks.createAccount._id || '',
          client_id: Cobuild.config.authServer.clientId,
          client_secret: Cobuild.config.authServer.clientSecret
        };

        debug('going to delete account:',  params);

        let options = {
          url:`http://auth:1337/api/v1/accounts/${params.id}`,
          method:'DELETE',
          body:params,
          json:true
        };

        request(options)
        .then(response =>{
          cb(null, response)
        })
        .catch(error => {
          cb(null, error.message)
        })
      };

      tasks.push(deleteAccount);
    }

    if(achievedTasks.createIdentity){
     
      let deleteIdentity = (cb) => {

        let params = {
          id: achievedTasks.createIdentity.uid,
          client_id: Cobuild.config.authServer.clientId,
          client_secret: Cobuild.config.authServer.clientSecret
        };

        debug('going to delete identity:',  params);

        let options = {
          url:`http://iam:1337/api/v1/identities/${params.id}`,
          method:'DELETE',
          body:params,
          json:true,
          headers:{
              'Authorization':'Bearer ' + Cobuild.config.iam.adminToken
          }
        };

        request(options)
        .then(response =>{
          cb(null, response)
        })
        .catch(error => {
          cb(null, error.message)
        })
        
      }

      tasks.push(deleteIdentity);
    }

    if(achievedTasks.createUser){

      debug('going to delete user:',  achievedTasks.createUser.id);
    
      let deleteUser = (cb) => {
        exports.delete(achievedTasks.createUser.id)
        .then(user => {
          cb(null, user)  
        })
        .catch(error => {
          cb(null, error.message)
        })
        
      }
      tasks.push(deleteUser);
    }

    if(_.isEmpty(tasks)){
      return resolve();
    }

    async.parallel(tasks, (err, result) => {
      if(err){
        return reject(err);
      }
      resolve(result);
    })  
  });
  
}

exports.list = function list(requestData){

  var pagination = {};
  
  if(requestData.query.limit){
    pagination.limit = parseInt(requestData.query.limit)
    pagination.page = parseInt(requestData.query.page) || 1
    pagination.offset = (pagination.page - 1) * pagination.limit;

    delete requestData.query.limit;
    delete requestData.query.page;
  }


  var query = Cobuild.Utils.Request.formatQueryString(requestData.query);
  var sort = Cobuild.Utils.Request.formatSortString(requestData.query.sort);

  if(!_.isEmpty(sort)){
    pagination.sort = sort;
    delete requestData.query.sort;
  }
  
  var id = requestData.params.id || requestData.query.id;
  
  if(id !== undefined) {
    query.id = id;
  }


  return new Promise(function(resolve, reject){
  
    var count = User
                .count('*')
                .from('user')
                .where(query)
                .first();


    var find = User
                .from('user')
                .where(query)

    if(!_.isEmpty(pagination)){
      find
      .limit(pagination.limit)
      .offset(pagination.offset)
    }

    if(pagination.sort){
      find
      .orderBy(pagination.sort);
    }

    Promise.all([count, find])
    .then(function(values){

      var paging = {
        totalItems:values[0].count || 0,
        limit: pagination.limit || -1
      };

      resolve({data:values[1], paging:paging});

    })
    .catch(reject);

  });
  

};

exports.cursor = function cursorFn(requestData){
  
  var query = Cobuild.Utils.Request.formatQueryString(requestData.query);
  var sort = {};
  var limit = parseInt(requestData.query.limit) || 10;

  delete query.limit;

  var id = requestData.query.max || requestData.query.since;

  return new Promise(function(resolve, reject){
      User
      .where({id:id})
      .from('user')
      .first()
      .then(function(user){
            
        if(!user){
          return reject(new Error('Object with id:',id,' not found'));
        }

        if(requestData.query.max){
          
          query.andWhere('cursor','<',user.cursor);
          query.orderBy('cursor desc');
          delete query.max;

        }else if(requestData.query.since){
          
          query.andWhere('cursor','>', user.cursor);
          query.orderBy('cursor asc');
          delete query.since;

        }

        User
        .from('user')
        .where(query)
        .limit(limit)
        .then(function(result){

            var paging = {
              next:'',
              prev:'',
              current: id
            };

            resolve({data:result, paging:paging});
          
        })
        .catch(function(err){
          reject(err);
        });

    })
    .catch(function(err){
      reject(err);
    });

  });

};

exports.update = function(id, data){

  return User('user')
    .update(data)
    .where({id:id})
    .then(function(updated) {
      
      return User
      .from('user')
      .where({id:id})
      .first()
      
    });

};


exports.delete = function(id){
  return User('user')
  .where({ id: id })
  .del()
  .then(function(result) {
    
    return User
    .from('user')
    .where({id:id})
    .first()
    
  });
  
}


exports.activateUser = function(id){
  return User('user')
  .update({active: true})
  .where({uid:id})
  .then(function(updated) {
    
    return User
    .from('user')
    .where({uid:id})
    .first()
    
  });
}

/**
 * Create confirmation email
 * @param  {Object} data
 * @param  {Object} userAgent
 */
exports.forgotPassword = function forgotPassword(data, userAgent) {
  return new Promise(function (resolve, reject) {

    let validateUser = (cb) => {
      if (!data.email) {
        let error = getError('email is required', 'forgotPassword', 'email_its_empty', 104, 'db');
        return cb(error);
      }
      User
        .where({ email: data.email })
        .from('user')
        .first()
        .then(user => cb(null, user))
        .catch(err => {
          let error = getError(err, 'forgotPassword', 'server_error', 500);
          cb(error)
        });
    };

    let createCode = ['user', (cb, prevData) => {

      if (!prevData || !prevData.user) {
        let error = getError('user not found', 'createCode', 'unknown_email', 105, 'db');
        return cb(error);
      }
      let params = _.pick(data, ['email']);
      params.client_id = config.authServer.clientId;
      params.client_secret = config.authServer.clientSecret;

      request({
        url: 'http://auth:1337/api/v1/forgotPassword',
        method: 'POST',
        body: params,
        json: true
      })
      .then(function (response) {
        debug("forgot password response", response);
        prevData.user.authorizationCode = response.code;
        cb(null, prevData);
      })
      .catch(err => {
        debug('forgotPassword err:', err.message);
        let error = getError(err, 'createCode', 'user_not_found', 101);
        cb(error);
      });

    }];

    let sendEmail = ['code', (cb, prevData) => {



      debug('forgotPassword prevData:', prevData);

      prevData = prevData.code;

      if(!config.url_notification || config.url_notification == ''){
        return cb(null);
      }

      let mobile = false;

      if (userAgent && (userAgent.indexOf('Dalvik') !== -1 || userAgent.indexOf('ART') !== -1 || userAgent.indexOf('Darwin') !== -1)) {
        mobile = true;
      }

      let link = `${config.FE.host}reset-password/${prevData.user.authorizationCode}`;

      let emailData = {
        to: prevData.user.email,
        templateName: 'template',
          templateContent: 'template-button',
        subject: i18n.__('reset_password'),
        vars: config.emailTemplateVars,
        templateVars: {
          greeting: i18n.__('greeting'),
          user: prevData.user.name || prevData.user.email,
          linkUrl: link,
          linkName: i18n.__('reset_password'),
          preMessage: i18n.__('pre_message_reset_password'),
          posMessage: i18n.__('pos_message_reset_password') + link
        }
      };

      request.post({
        url: config.url_notification,
        json: emailData
      },
      function (err, response, body) {
        debug(err, body);
        if (err) {
          err = getError(err, 'forgotPassword', 'invalid_request', 102);
        }
        cb(err, body);
      });
    }];

    let tasks = {
      user: validateUser,
      code: createCode,
      notification: sendEmail
    };

    async.auto(tasks, (err, result) => {

      if (err) {
        debug('forgot password err:', err);

        return reject(err);
      }

      return resolve({ message: i18n.__('code_sent'), userData: result.user.authorizationCode});

    });

  });
};

function getError(err, serverInfo, message, code, type) {
  let error = {
    data: err.error || err,
    serverInfo: serverInfo
  };
  if (message && (err.statusCode === 400 || type === 'db')) {
    error.userMessage = i18n.__(message);
    error.code = code;
  } else {
    error.userMessage = i18n.__('server_error');
    error.code = 500;
  }
  return error;
}

exports.resetPassword = function resetPassword(code, body) {
  return new Promise(function (resolve, reject) {

    let params = {
      code: body.code,
      newPassword: body.password,
      client_id: config.authServer.clientId,
      client_secret: config.authServer.clientSecret
    };
    request({
      url: 'http://auth:1337/api/v1/resetPassword',
      method: 'POST',
      body: params,
      json: true
    })
    .then((response) => {
      debug("reset password response", response);
      resolve({
        message: i18n.__('changed_password')
      });
    })
    .catch(err => {
      debug("reset password error", err);
      let error = getError(err, 'resetPassword', 'invalid_request', 103);
      reject(error);
    });

  });
};

exports.statusUser = function(email){
  return new Promise(function(resolve, reject){
    User
    .from('user')
    .where({email:email})
    .first()
    .then((response)=>{
      var active = (response) ? response.active  : true;
      resolve(active);
    })
    .catch((err) =>{
      reject(err);
    })
  });
};

exports.getUid = function(email){
   return new Promise(function(resolve, reject){
    User
    .from('user')
    .where({email:email})
    .first()
    .then((response)=>{
     resolve(response)
    })
    .catch((err) =>{
      reject(err);
    })
  });
}

exports.getPolicies = function(uid){
  return new Promise(function(resolve, reject){
    var url = 'http://iam:1337/api/v1/identities/'+uid+'/payload';
    request.get(url,(error, response, body) => {
      if(error || !body) return reject({errors:"Could not load account"});
      var body = JSON.parse(body)
      if(error || !body || !body.data) 
        return reject({errors:"Could not load account"});
                            
      var scopes = _.uniq(
        _.flatten(
          _.map(body.data.policies,(e)=>{
            return _.map(e.statements,(a)=>{
              return a.action
            })
          })
        )
      )
      resolve(scopes)
    });
  });
};


exports.assignPassword = function(uid, data){
  return new Promise(function(resolve, reject){
    
    var findUser = function(cb){
      exports.assignedUser(uid)
      .then(function(response){
        cb(null, response)
      })
      .catch(cb);
    }

    var loginUser = function(cbData, cb){
      var params = {
        email: cbData.email,
        password: utils.generatePassword(cbData.email)
      }

      authLogin(params, cb);
    }

    var updateUser = function(cbData, cb){
      let params              = _.pick(data, 'password');
      params.password         = bcrypt.hashSync(params.password);
      params.client_id        = Cobuild.config.authServer.clientId;
      params.client_secret    = Cobuild.config.authServer.clientSecret;
  
      request({
          url:'http://auth:1337/api/v1/user',
          method:'PUT',
          body: params,
          json:true,
          headers:{
            'Authorization':'Bearer ' + cbData.access_token
          }
      })
      .then(function(response){
       cb(null, cbData)
      })
      .catch(cb);
    };

    var tasks = [
      findUser,
      loginUser,
      updateUser
    ]

    async.waterfall(tasks, (err, response)=>{
      if(err) return reject(err);
      resolve(response);
    })
    
  })
}

exports.assignedUser = function (uid){
  return new Promise(function(resolve, reject){
    User
    .from('user')
    .where({uid:uid})
    .first()
    .then((response)=>{
      resolve(response);
    })
    .catch((err) =>{
      reject(err);
    })
  })
};


exports.getUserCompanyId = function(uid){
  return new Promise(function(resolve, reject){

    var getUser = (cb)=>{
      exports.assignedUser(uid)
      .then((response)=>{
        cb(null, response)
      })
      .catch((err)=>{
        cb(err);
      })
    }

    var getCompanyId = (cbData, cb)=>{
      var user = cbData;
      UserCompany
      .select([
        'company.id'
      ])
      .from('user_company')
      .innerJoin('user', 'user.id', 'user_company.user')
      .innerJoin('company', 'company.id', 'user_company.company')
      .where({user: user.id})
      .first()
      .then((response)=>{
        cb(null,response)
      })
      .catch(cb)
    }

    var tasks = [
      getUser,
      getCompanyId
    ]

    async.waterfall(tasks, (err, response)=>{
      if(err) return reject(err);

      resolve(response)
    })
   
  });
}


exports.getCompanyUsers = function(requestData){
  var pagination = {};
  
  if(requestData.query.limit){
    pagination.limit = parseInt(requestData.query.limit)
    pagination.page = parseInt(requestData.query.page) || 1
    pagination.offset = (pagination.page - 1) * pagination.limit;

    delete requestData.query.limit;
    delete requestData.query.page;
  }


  var query = Cobuild.Utils.Request.formatQueryString(requestData.query);
  var sort = Cobuild.Utils.Request.formatSortString(requestData.query.sort);

  if(!_.isEmpty(sort)){
    pagination.sort = sort;
    delete requestData.query.sort;
  }
  
  var id = requestData.params.id || requestData.query.id;
  
  if(id !== undefined) {
    query.id = id;
  }

  query.company = requestData.params.companyId;



  return new Promise(function(resolve, reject){
  
    var count = User
                .count('*')
                .from('user_company')
                .where(query)
                .whereNot({role: 'owner'})
                .first();

    var rowUserId = User.raw('??', ['user.uid']);

    var countReports = Report
                      .count('*')
                      .from('report')
                      .where('report.user',rowUserId)
                      .as('reportsNumber');

    var lastReport =  Report
                      .max('created_at')
                      .from('report')
                      .where('report.user',rowUserId)
                      .as('lastReport');

    var find =  UserCompany
                .select([
                  'user.*',
                  'user_company.role',
                  'user_company.id AS uc_id',
                  countReports,
                  lastReport
                ])
                .from('user_company')
                .innerJoin('user', 'user.id', 'user_company.user')
                .innerJoin('company', 'company.id', 'user_company.company')
                .where(query)
                .whereNot({role: 'owner'})

    var countAdmins = User
                      .count('*')
                      .from('user_company')
                      .where(_.merge(query,{role: 'admin'}))
                      .first()

    if(!_.isEmpty(pagination)){
      find
      .limit(pagination.limit)
      .offset(pagination.offset)
    }

    if(pagination.sort){
      find
      .orderBy(pagination.sort);
    }

    Promise.all([count, find, countAdmins])
    .then(function(values){

      var paging = {
        totalItems: _.isEmpty(values[0]) ? 0 : values[0].count,
        limit: parseInt(pagination.limit) || -1,
        page: pagination.page
      };

      paging.totalPages = Math.ceil(paging.totalItems / paging.limit);

      resolve({data:values[1], paging:paging, totalAdmins: values[2]});

    })
    .catch(reject);

  });
}

exports.getReportsByUser = function (requestData, uid){
  var pagination = {};
  
  if(requestData.query.limit){
    pagination.limit = parseInt(requestData.query.limit)
    pagination.page = parseInt(requestData.query.page) || 1
    pagination.offset = (pagination.page - 1) * pagination.limit;

    delete requestData.query.limit;
    delete requestData.query.page;
  }


  var query = Cobuild.Utils.Request.formatQueryString(requestData.query);
  var sort = Cobuild.Utils.Request.formatSortString(requestData.query.sort);

  if(!_.isEmpty(sort)){
    pagination.sort = sort;
    delete requestData.query.sort;
  }
  
  var id = requestData.params.id || requestData.query.id;
  
  if(id !== undefined) {
    query.id = id;
  }

  query.user = uid;

  return new Promise(function(resolve, reject){
  
    var count = Report
                .count('*')
                .from('report')
                .where(query)
                .first();

  

    var find =  Report
                .select([
                  'created_at as createdAt',
                  'survey as survey',
                  'uid as uid'
                ])
                .from('report')
                .where(query)
                .orderBy('created_at', 'desc')

    var survey = exports.getSurveyInfo(uid)

    if(!_.isEmpty(pagination)){
      find
      .limit(pagination.limit)
      .offset(pagination.offset)
    }

    if(pagination.sort){
      find
      .orderBy(pagination.sort);
    }

    Promise.all([count, find, survey])
    .then(function(values){

       var paging = {
        totalItems: _.isEmpty(values[0]) ? 0 : values[0].count,
        limit: parseInt(pagination.limit) || -1,
        page: pagination.page
      };

      paging.totalPages = Math.ceil(paging.totalItems / paging.limit);

      resolve({data: matchValues(values), paging:paging});

    })
    .catch(reject);

  });
};

exports.getSurveyInfo = function(uid){
  return new Promise(function(resolve, reject){
    request({
      url:'http://survey:1337/api/v1/surveys/information/' + uid,
      method:'GET',
      headers: {
        'Authorization':'Bearer ' + config.iam.adminToken
      }
    })
    .then(function(response){
      resolve(response)
    })
    .catch(function(err){
      reject(err);
    });
  });
};


function matchValues(values){
  var data = [];
  var surveys = {}
  if(values.length != 3) 
    return data;

  if(_.isEmpty(values[1]) || _.isEmpty(values[2]))
    return data;

  var reports = values[1];

  try{
    surveys = JSON.parse(values[2]);
  }catch(e){
    return data;
  }

  surveys = surveys.data;
  

  reports.forEach((report)=>{
    survey = _.find(surveys, (surveyData)=>{
      return surveyData.id == report.survey;
    })

    if(survey && survey.questions) {
      if(survey.questions['basic.company'])
        report.companyName = survey.questions['basic.company']
      else if (survey.questions['individual.company'])
        report.companyName = survey.questions['individual.company']
      else if (survey.questions['company.company'])
        report.companyName = survey.questions['company.company']
    }
    else {
      report.companyName = ''
    }

    data.push(report);
  })

  return data;

}

exports.changeRole = function(role, payload){
  return new Promise(function(resolve, reject){
    var updateUser = function(cb){
     
      UserCompany('user_company')
      .update({role: role})
      .where({id: payload.id})
      .then((response)=>{
        User
        .from('user')
        .where({id: payload.user})
        .first()
        .then((user)=>{
          cb(null, user);
        })
        .catch(cb)
      })
      .catch(cb)
    }

    var updateIdentity = function(cbData, cb){
      var uid = cbData.uid;
      let policies = {
        'admin': ['srn:iam:default:policies:company-owner'],
        'user': ['srn:iam:default:policies:company-user']
      }

      let payload = {
        policies: policies[role]
      };

      request({
          url:'http://iam:1337/api/v1/identities/' + uid,
          method:'PUT',
          body: payload,
          json:true,
          headers:{
            'Authorization':'Bearer ' + Cobuild.config.iam.adminToken
          }
      })
      .then(function(response){
        cb(null, response)
      })
      .catch(cb);
    }

    var tasks = [
      updateUser, 
      updateIdentity
    ]

    async.waterfall(tasks, (err, response)=>{
      if(err) return reject(err);

      resolve(response)
    })
  });
}

function authLogin(data, cb){
  let authService = Cobuild.Utils.Files.requireIfExists('/app/modules/auth/services/authService');
  let params  = _.pick(data, ['email','password']);

  authService.login(params)
  .then(function(response){
    cb(null, response)
  })
  .catch(function(err){
    cb(err)
  })
}


function findByEmail(email){
  return new Promise(function(resolve, reject){

    User
    .from('user')
    .where({email: email})
    .first()
    .then((user)=>{
      resolve(user);
    })
    .catch( err => reject(err))
  })
}

exports.getAccount = function getAccount(uid) {
  
  const query = { uid };
  return User
    .from('user')
    .where(query)
    .first()
    .then(function(response) {
      return { data: response };
    });
};
