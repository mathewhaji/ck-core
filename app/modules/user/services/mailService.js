const async           = require('async');
const i18n            = require('i18n');
const Cobuild         = require('cobuild2');
const _               = require('lodash');

const utilsMail       = Cobuild.Utils.Files.requireIfExists('/app/utils/mail');

var exports           = module.exports;

exports.sendActivation = function (user) {
  return new Promise(function(resolve, reject) {
    const link = `${Cobuild.config.FE.host}/activate-user/${user.uid}`;
    const payload = {
      to: user.email,
      vars: Cobuild.config.emailTemplateVars, 
      subject: i18n.__('activate_account_subject'),
      templateName: 'template',
      templateContent: 'template-button',
      templateVars: {
        greeting: i18n.__('greeting', {
          user: user.email
        }),
        linkUrl: link,
        linkName: i18n.__('activate_button'),
        message: [
          i18n.__('activate_account_message1')
        ],
        after: [
          i18n.__('contact_us'),
          i18n.__('thanks'),
          i18n.__('farewell')
        ]
      }
    };

    utilsMail.sendMail(payload)
    .then(response=>resolve(response))
    .catch(err=>reject(err));
  })
}
exports.sendAssignedUser = function(user, companyId, fromUser){
  return new Promise(function(resolve, reject){
    const getCompany = (cb)=>{
      const companyService = Cobuild.Utils.Files.requireIfExists('/app/modules/company/services/companyService');
      
      companyService
      .getCompany(companyId)
      .then(response=>cb(null, response))
      .catch(cb)
    }

    const getUser = (cb)=>{
      if(!fromUser) return cb(null, {});

      const userService = Cobuild.Utils.Files.requireIfExists('/app/modules/user/services/userService');
      userService
      .assignedUser(fromUser)
      .then(response=>cb(null, response))
      .catch(cb)
    }

    const sendMail = ['company', 'from', (cb, cbData)=>{
  
      const company = cbData.company;
      const from = cbData.from;

      const link = `${Cobuild.config.FE.host}/assign-password/${user.uid}`;

      const subject = i18n.__('assign_company_subject', {
        company: company.name,
        from:  (_.isEmpty(from)) ? '' : `from ${from.email}`
      });

      const payload = {
        to: user.email,
        subject: subject,
        templateName: 'template',
        templateContent: 'template-button',
        templateVars: {
          logo: company.logo_url,
          linkUrl: link,
          linkName: i18n.__('assign_company_button'),
          message:[
            i18n.__('assign_company_message1', {
              company: company.name,
              from:  (_.isEmpty(from)) ? '' : `by ${from.email}`
            }),
            i18n.__('assign_company_message2'),
            i18n.__('assign_company_message3')
          ] 
        }
      };
  
      utilsMail.sendMail(payload)
      .then(response=>cb(null, response))
      .catch(cb);
    }]
    
    const tasks = {
      company: getCompany,
      from: getUser,
      mail: sendMail
    };

    async.auto(tasks, (err, response)=>{
      if(err) return reject(err);

      resolve(response);
    })
  });
}