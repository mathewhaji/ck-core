'use strict';
/* jshint undef: false */
/* jshint unused: false */
/* global describe it before async f request */

if(!process.env.NODE_PATH){
  throw new Error("NODE_PATH environment variable is required");
}

var Cobuild         = require('cobuild2');
var fixtures        = Cobuild.Utils.Test.loadFixtures(__dirname);
var request         = require('supertest');
var request         = request('http://localhost:3000');
var should          = require('should');
var path            = require('path');
var User  = require(path.join(__dirname,'..','..','models/user'));

console.log('fixtures',fixtures)

describe('User Tests', function() {

  var modelId; 

  it('should be able to reach health endpoint', function(done) {
    this.timeout(10000);

    request
      .get('/api/'+Cobuild.config.apiVersion+'/users/health')
      .expect(200)
      .end(done);
  });


  it('should be able to reach list endpoint', function(done) {
    this.timeout(10000);

    request
      .get('/api/'+Cobuild.config.apiVersion+'/users')
      .expect(200)
      .end(function(err, res){
        should.not.exist(err);
        res.body.should.have.property('data');
        res.body.should.have.property('paging');
        done(err);
      });
  });

  

  it('should  be able to insert a User', function(done) {
    this.timeout(10000);

    request
      .post('/api/'+Cobuild.config.apiVersion+'/users')
      .send(fixtures.create)
      .expect(200)
      .end(function(err, res){
        should.not.exist(err);
        console.log("insert response:", res.body);
        modelId = res.body.data.id  
        done(err);
      });
  });

  it('should be able to reach read endpoint', function(done) {
    this.timeout(10000);
    if(!modelId){
      done(new Error('No model id'))
    }
    request
      .get('/api/'+Cobuild.config.apiVersion+'/users/'+modelId)
      .expect(200)
      .end(function(err, res){
        should.not.exist(err);
        res.body.data.name.should.be.equal(fixtures.create.name);
        
        done(err);
      });
  });

  it('should not be able to insert a User', function(done) {
    this.timeout(10000);

    request
      .post('/api/'+Cobuild.config.apiVersion+'/users')
      .send(fixtures.createWrong)
      .expect(400)
      .end(function(err, res){

        done(err);
      });
  });

  it('should not be able to update a User', function(done) {
    
    this.timeout(10000);

    delete fixtures.update.id;

    request
      .put('/api/'+Cobuild.config.apiVersion+'/users')
      .send(fixtures.update)
      .expect(400)
      .end(function(err, res){
        done(err);
      });
  });

  it('should be able to update a User', function(done) {
    
    this.timeout(10000);

    fixtures.update.id = modelId;

    console.log("update fixtures:", fixtures.update);

    request
      .put('/api/'+Cobuild.config.apiVersion+'/users')
      .send(fixtures.update)
      .expect(200)
      .end(function(err, res){
        res.body.data.name.should.be.equal('Hello');
        done(err);
      });
  });

  it('should be able to update a User with id on URL', function(done) {
    
    this.timeout(10000);

    

    request
      .put('/api/'+Cobuild.config.apiVersion+'/users/'+modelId)
      .send(fixtures.update)
      .expect(200)
      .end(function(err, res){
        res.body.data.name.should.be.equal('Hello');
        done(err);
      });
  });

  it('should not be able to destroy a User', function(done) {
    this.timeout(10000);

    request
      .delete('/api/'+Cobuild.config.apiVersion+'/users')      
      .expect(400)
      .end(function(err, res){
        done(err);
      });
  });

  it('should be able to destroy a User', function(done) {
    this.timeout(10000);
    
    request
      .delete('/api/'+Cobuild.config.apiVersion+'/users')
      .send({id:modelId})     
      .expect(200)
      .end(function(err, res){
        done(err);
      });
  });

  it('should be able to destroy a User using url parameter', function(done) {
    this.timeout(10000);
    
    request
      .delete('/api/'+Cobuild.config.apiVersion+'/users/'+modelId)
      .expect(200)
      .end(function(err, res){
        done(err);
      });
  });


  

});