var Cobuild           = require('cobuild2');
var fs                = require('fs');
var knex              = require('knex');
var path              = require('path');
var directory         = path.resolve(__dirname + '/../migrations')

var client = knex({
  client: process.env.DB_TYPE,
  connection: process.env.DB_URL,
  pool: { 
    min: 0, 
    max: process.env.DB_MAX_CONNECTIONS || 6 
  },
  debug:false,
  migrations:{
    directory:directory
  }
});



client.schema.hasTable('user').then(function(exists) {
  if (!exists) {
    client.migrate.latest()
    .then(function(argument) {
      console.log('finish migration')
    });
    
  }
});

module.exports = client;