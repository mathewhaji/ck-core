var Cobuild           = require('cobuild2');
var _                 = require('lodash');
var async             = require('async');
var Category    = require(Cobuild.Utils.Files.dirpath(__dirname)+'/models/category');

var exports           = module.exports;



exports.create = function(data){
  return new Promise(function(resolve, reject){
      Category('category')
      .insert(data,'*')
      .then(function(result) {
        console.log(result);
        resolve(result.shift());
      })
      .catch(reject);
  })
};

exports.list = function list(requestData){

  var pagination = {};
  
  if(requestData.query.limit){
    pagination.limit = parseInt(requestData.query.limit)
    pagination.page = parseInt(requestData.query.page) || 1
    pagination.offset = (pagination.page - 1) * pagination.limit;

    delete requestData.query.limit;
    delete requestData.query.page;
  }


  var query = Cobuild.Utils.Request.formatQueryString(requestData.query);
  var sort = Cobuild.Utils.Request.formatSortString(requestData.query.sort);

  if(!_.isEmpty(sort)){
    pagination.sort = sort;
    delete requestData.query.sort;
  }
  
  var id = requestData.params.id || requestData.query.id;
  
  if(id !== undefined) {
    query.id = id;
  }


  return new Promise(function(resolve, reject){
  
    var count = Category
                .count('*')
                .from('category')
                .where(query)
                .first();


    var find = Category
                .from('category')
                .where(query)

    if(!_.isEmpty(pagination)){
      find
      .limit(pagination.limit)
      .offset(pagination.offset)
    }

    if(pagination.sort){
      find
      .orderBy(pagination.sort);
    }

    Promise.all([count, find])
    .then(function(values){

      var paging = {
        totalItems:values[0].count || 0,
        limit: pagination.limit || -1
      };

      resolve({data:values[1], paging:paging});

    })
    .catch(reject);

  });
  

};

exports.cursor = function cursorFn(requestData){
  
  var query = Cobuild.Utils.Request.formatQueryString(requestData.query);
  var sort = {};
  var limit = parseInt(requestData.query.limit) || 10;

  delete query.limit;

  var id = requestData.query.max || requestData.query.since;

  return new Promise(function(resolve, reject){
      Category
      .where({id:id})
      .from('category')
      .first()
      .then(function(category){
            
        if(!category){
          return reject(new Error('Object with id:',id,' not found'));
        }

        if(requestData.query.max){
          
          query.andWhere('cursor','<',category.cursor);
          query.orderBy('cursor desc');
          delete query.max;

        }else if(requestData.query.since){
          
          query.andWhere('cursor','>', category.cursor);
          query.orderBy('cursor asc');
          delete query.since;

        }

        Category
        .from('category')
        .where(query)
        .limit(limit)
        .then(function(result){

            var paging = {
              next:'',
              prev:'',
              current: id
            };

            resolve({data:result, paging:paging});
          
        })
        .catch(function(err){
          reject(err);
        });

    })
    .catch(function(err){
      reject(err);
    });

  });

};

exports.update = function(id, data){

  return Category('category')
    .update(data)
    .where({id:id})
    .then(function(updated) {
      
      return Category
      .from('category')
      .where({id:id})
      .first()
      
    });

};


exports.delete = function(id){
  return Category('category')
  .where({ id: id })
  .del()
  .then(function(result) {
    
    return Category
    .from('category')
    .where({id:id})
    .first()
    
  });
  
}