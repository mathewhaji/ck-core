var Cobuild = require('cobuild2');
var _       = require('lodash');
var fs      = require('fs');

var data = fs.readFileSync(Cobuild.Utils.Files.dirpath(__dirname)+'/models/category.json', {encoding: 'utf8'});
var modelJSON = JSON.parse(data);


exports.up = function(knex, Promise) {
  return knex.schema.createTable(modelJSON.name, function(table){
       table.increments('id').primary();

       _.forEach(modelJSON.schema, function(fieldConfig, fieldName){
          
          var type = fieldConfig.type.toLowerCase();
          var field = table[type](fieldName)
          if(fieldConfig.required){
            field.notNull()
          }
       });

       table.timestamp('created_at').defaultTo(knex.fn.now());

  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable(modelJSON.name);
};
