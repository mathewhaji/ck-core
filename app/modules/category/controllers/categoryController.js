var Cobuild                     = require('cobuild2');
var _                           = require('lodash');
var categoryService    = require(Cobuild.Utils.Files.dirpath(__dirname)+'/services/categoryService');

var exports                     = module.exports;

exports.create = function(req, res) {
  
  categoryService.create(req.body)
  .then(function(record) {
    res.ok(record)
  })
  .catch(function(err){
    res.badRequest(err);
  });

};

exports.list = function(req, res) {
  
  var id = req.params.id || req.query.id;

  categoryService
  .list(req)
  .then(
    
    function success(result){

      if(id !== undefined){
        res.ok((result && result.data.length > 0)? result.data[0] : {});
      }else{
        res.ok(result);  
      }
    },

    function error(err){
      res.badRequest(err);
    });
  

};

exports.cursor = function(req, res){



  var id = req.params.id || req.query.id;
  console.log('cursor:',id);

  categoryService
  .cursor(req)
  .then(
    
    function success(result){
      if(id !== undefined){
        res.ok((result && result.data.length > 0)? result.data[0] : {});
      }else{
        res.ok(result);  
      }
    },

    function error(err){
      res.badRequest(err);
    });
};


exports.update = function(req, res){

  var id = req.body.id || req.body._id  || req.params.id;

  //console.log("id to update:", id)
  var data = _.clone(req.body);
  delete data.id;

  categoryService
  .update(id, data)
  .then(function (record) {
    res.ok(record);
  })
  .catch(function(err){
    res.badRequest(err);
  });
  
  

};

exports.destroy = function(req, res) {
  
  console.log("req.body:", req.body);
  console.log("req.params:", req.params);

  var id = req.body.id || req.params.id;
  console.log("delete id:", id);

  categoryService
  .delete(id)
  .then(function (record) {
    res.ok(record);
  })
  .catch(function(err){
    res.badRequest(err);
  });

};

