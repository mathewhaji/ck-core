'use strict';
/* jshint undef: false */
/* jshint unused: false */
/* global describe it before async f request */

if(!process.env.NODE_PATH){
  throw new Error("NODE_PATH environment variable is required");
}

var Cobuild         = require('cobuild2');
var fixtures        = Cobuild.Utils.Test.loadFixtures(__dirname);
var request         = require('supertest');
var request         = request('http://localhost:3000');
var should          = require('should');
var path            = require('path');
var Category  = require(path.join(__dirname,'..','..','models/category'));

console.log('fixtures',fixtures);

describe('Category Tests', function() {

  var modelId; 

  it('should be able to reach health endpoint', function(done) {
    this.timeout(10000);

    request
      .get('/api/'+Cobuild.config.apiVersion+'/categories/health')
      .expect(200)
      .end(done);
  });


  it('should be able to reach list endpoint', function(done) {
    this.timeout(10000);

    request
      .get('/api/'+Cobuild.config.apiVersion+'/categories')
      .expect(200)
      .end(function(err, res){
        should.not.exist(err);
        res.body.should.have.property('data');
        res.body.should.have.property('paging');
        done(err);
      });
  });

  

  it('should  be able to insert a Category', function(done) {
    this.timeout(10000);

    request
      .post('/api/'+Cobuild.config.apiVersion+'/categories')
      .send(fixtures.create)
      .expect(200)
      .end(function(err, res){
        should.not.exist(err);
        console.log("insert response:", res.body);
        modelId = res.body.data.id  
        done(err);
      });
  });

  it('should be able to reach read endpoint', function(done) {
    this.timeout(10000);
    if(!modelId){
      done(new Error('No model id'))
    }
    request
      .get('/api/'+Cobuild.config.apiVersion+'/categories/'+modelId)
      .expect(200)
      .end(function(err, res){
        should.not.exist(err);
        res.body.data.name.should.be.equal(fixtures.create.name);
        
        done(err);
      });
  });

  it('should not be able to insert a Category', function(done) {
    this.timeout(10000);

    request
      .post('/api/'+Cobuild.config.apiVersion+'/categories')
      .send(fixtures.createWrong)
      .expect(400)
      .end(function(err, res){

        done(err);
      });
  });
/*
  it('should be able to paginate a Category using cursor', function(done) {
    this.timeout(10000);

    console.log('/api/'+Cobuild.config.apiVersion+'/categories/cursor/?since='+modelId);

    // before(function(cb){
    //   console.log("creating one for cursor paging")
    //   Category.create(fixtures.create, cb);
    // })

    request
      .get('/api/'+Cobuild.config.apiVersion+'/categories/cursor/?since='+modelId)
      .expect(200)
      .end(function(err, res){
        console.log("read result:",res.body);
        res.body.should.have.property('paging');
        res.body.paging.should.have.property('prev');
        res.body.paging.should.have.property('next');
        done(err);
      });
  });
*/
  it('should not be able to update a Category', function(done) {
    
    this.timeout(10000);

    delete fixtures.update.id;

    request
      .put('/api/'+Cobuild.config.apiVersion+'/categories')
      .send(fixtures.update)
      .expect(400)
      .end(function(err, res){
        done(err);
      });
  });

  it('should be able to update a Category', function(done) {
    
    this.timeout(10000);

    fixtures.update.id = modelId;

    console.log("update fixtures:", fixtures.update);

    request
      .put('/api/'+Cobuild.config.apiVersion+'/categories')
      .send(fixtures.update)
      .expect(200)
      .end(function(err, res){
        res.body.data.name.should.be.equal('GDPR');
        done(err);
      });
  });

  it('should be able to update a Category with id on URL', function(done) {
    
    this.timeout(10000);

    

    request
      .put('/api/'+Cobuild.config.apiVersion+'/categories/'+modelId)
      .send(fixtures.update)
      .expect(200)
      .end(function(err, res){
        res.body.data.name.should.be.equal('GDPR');
        done(err);
      });
  });

  it('should not be able to destroy a Category', function(done) {
    this.timeout(10000);

    request
      .delete('/api/'+Cobuild.config.apiVersion+'/categories')      
      .expect(400)
      .end(function(err, res){
        done(err);
      });
  });

  it('should be able to destroy a Category', function(done) {
    this.timeout(10000);
    
    request
      .delete('/api/'+Cobuild.config.apiVersion+'/categories')
      .send({id:modelId})     
      .expect(200)
      .end(function(err, res){
        done(err);
      });
  });

  it('should be able to destroy a Category using url parameter', function(done) {
    this.timeout(10000);
    
    request
      .delete('/api/'+Cobuild.config.apiVersion+'/categories/'+modelId)
      .expect(200)
      .end(function(err, res){
        done(err);
      });
  });


  

});