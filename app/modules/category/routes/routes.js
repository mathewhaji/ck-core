var Cobuild           = require('cobuild2');
var express           = require('express');
var router            = express.Router();
var categoryController   = require(Cobuild.Utils.Files.dirpath(__dirname)+'/controllers/categoryController');
var categoryValidator    = require(Cobuild.Utils.Files.dirpath(__dirname)+'/validators/category');
module.exports = function(app, limiter) {
  
/**
  *   Limiter
  *     For more info see: https://www.npmjs.com/package/express-limiter
  */
  if (limiter){
    // Here insert the rules for use limiter.
    // Example
    limiter({
      path: '/api/'+Cobuild.config.apiVersion+'/categories/health',
      method: 'get',
      lookup: ['connection.remoteAddress'],
      // 10 requests per 10 seconds
      total: 10,
      expire: 1000 * 10 ,
      onRateLimited: function (req, res, next) {
        next({ message: 'Rate limit exceeded', status: 400 })
      }
    });
  };
  
/**
  * The Policies load when init a server, in that policies are some validations like:
  * isLoggedIn or isAdmin
  * To use:
  * 
  * var authPolicyIsLoggedIn      = Cobuild.Policies.getPolicy('cobuild.'+config.authentication.strategy+'.isLoggedIn');
  * var authPolicyIsAdmin      = Cobuild.Policies.getPolicy('cobuild.'+config.authentication.strategy+'.isAdmin');
  * 
  * On the declaretion of end-point call a policy
  *
  * router.put('/', authPolicyIsLoggedIn, Ctrl.update);
  *
  */
  
	router.get('/health', function(req, res) {
    
     return res.status(200).send('ok');
  });
/**
 * @api {post} /api/v1/categories Create a Category
 * @apiName Category - Create
 * @apiGroup Category
 * @apiDescription Create a Category into the database
 * @apiVersion 1.0.0
 *
 * @apiParam {String} title Risk category title
 * @apiParam {String} description Risk category description
 *
 * @apiParamExample {json} Request-Example:
 * {
*    "description":Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, debitis, labore aliquid porro natus maiores corporis minima quos distinctio tenetur eaque accusantium inventore aliquam. Deleniti ab animi error delectus sapiente.,
 *   "title": "Data Breach"
 * }
 *
 * @apiSuccessExample {JSON} response-example:
 * {
 *   "description":Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, debitis, labore aliquid porro natus maiores corporis minima quos distinctio tenetur eaque accusantium inventore aliquam. Deleniti ab animi error delectus sapiente.,
 *   "title": "Data Breach",
 *   "id": "1"
 * }
 */  
  router.post('/', categoryController.create);
/**
 * @api {get} /api/v1/categories Read records for a Category
 * @apiName Category - Read
 * @apiGroup Category
 * @apiDescription Read several Category given conditions
 * @apiVersion 1.0.0
 * 
 *
 * @apiParamExample {json} Request-Example:
 * {
 * }
 * 
 * @apiSuccessExample {JSON} response-example:* 
 * [
 *   {
 *      "description":Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, debitis, labore aliquid porro natus maiores corporis minima quos distinctio tenetur eaque accusantium inventore aliquam. Deleniti ab animi error delectus sapiente.,
 *      "title": "Data Breach",
 *      "id": "1"
 *    }
 *  ]
 */
  router.get('/', categoryController.list);
  
/**
 * @api {get} /api/v1/categories/cursor  Read :limit records up to :max id or :since id
 * @apiName Category - Cursor
 * @apiGroup Category
 * @apiDescription Read :limit Category given :max or :since ids
 * @apiVersion 1.0.0
 *
 * @apiParam {string} max id of the Category
 * @apiParam {string} since id of the Category
 * @apiParam {string} limit desired number  of the Category instances
 *
 * @apiParamExample {json} Request-Example:
 * {
 * }
 * 
* @apiSuccessExample {JSON} response-example:* 
 * [
 *   {
 *      "description":Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, debitis, labore aliquid porro natus maiores corporis minima quos distinctio tenetur eaque accusantium inventore aliquam. Deleniti ab animi error delectus sapiente.,
 *      "title": "Data Breach",
 *      "id": "1"
 *    }
 *  ]
 */
  router.get('/cursor', categoryController.cursor);
  
  
/**
 * @api {get} /api/v1/categories/:id  Read a record for a Category with given id
 * @apiName Category - Read
 * @apiGroup Category
 * @apiDescription Read one Category given id
 * @apiVersion 1.0.0
 *
 * @apiParam {string} id of the Category
 *
 * @apiParamExample {json} Request-Example:
 * {
 * }
 * 
 * @apiSuccessExample {JSON} response-example:* 
 * {
 *   "description":Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, debitis, labore aliquid porro natus maiores corporis minima quos distinctio tenetur eaque accusantium inventore aliquam. Deleniti ab animi error delectus sapiente.,
 *   "title": "Data Breach",
 *   "id": "1"
 * }
 */
  router.get('/:id', categoryController.list);
 
/**
 * @api {put} /api/v1/categories Update a Category
 * @apiName Category - Update
 * @apiGroup Category
 * @apiDescription Update a Category given their id
 * @apiVersion 1.0.0
 *
 * @apiParam {string} id of the Category
 *
 * @apiParamExample {json} Request-Example:
 * {
 *   "description":Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, debitis, labore aliquid porro natus maiores corporis minima quos distinctio tenetur eaque accusantium inventore aliquam. Deleniti ab animi error delectus sapiente.,
 *   "title": "Data Breach"
 * }
 *
 * @apiSuccessExample {JSON} response-example:
 * {
 *   "description":Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto, debitis, labore aliquid porro natus maiores corporis minima quos distinctio tenetur eaque accusantium inventore aliquam. Deleniti ab animi error delectus sapiente.,
 *   "title": "Data Breach",
 *   "id": "1"
 * }
 */ 
  router.put('/', categoryValidator.haveId, categoryController.update);
  router.put('/:id', categoryValidator.haveId, categoryController.update);
/**
 * @api {delete} /api/v1/categories Delete a Category instance
 * @apiName Category - Delete
 * @apiGroup Category
 * @apiDescription Delte a Category given their id
 * @apiVersion 1.0.0
 *
 * @apiParam {string} id of the Category to be deleted
 *
 * @apiParamExample {json} Request-Example:
 * {
 *   "id": "573f22b74db4608025e7abf4"
 * }
 *
 * @apiSuccessExample {JSON} response-example:
 * {
 *   "id": "573f22b74db4608025e7abf4"
 * }
 */ 
  router.delete('/', categoryValidator.haveId,categoryController.destroy);
  router.delete('/:id', categoryValidator.haveId, categoryController.destroy);
  
  console.log('/api/'+Cobuild.config.apiVersion+'/categories');
  app.use('/api/'+Cobuild.config.apiVersion+'/categories',router);
  
};
