var Cobuild           = require('cobuild2');
var express           = require('express');
var router            = express.Router();
var reports           = require(Cobuild.Utils.Files.dirpath(__dirname)+'/controllers/reportsController');
var signedUrlUtil     = Cobuild.Utils.Files.requireIfExists('/app/utils/signedUrl');
var reportsValidator  = require(Cobuild.Utils.Files.dirpath(__dirname)+'/validators/reports');
module.exports = function(app, limiter) {

  /**
  *   Limiter
  *     For more info see: https://www.npmjs.com/package/express-limiter
  */

  if (limiter){
    // Here insert the rules for use limiter.
    // Example
    limiter({
      path: '/api/'+Cobuild.config.apiVersion+'/reports/health',
      method: 'get',
      lookup: ['connection.remoteAddress'],
      // 10 requests per 10 seconds
      total: 10,
      expire: 1000 * 10 ,
      onRateLimited: function (req, res, next) {
        next({ message: 'Rate limit exceeded', status: 400 })
      }
    });
  }

  /**
  * The Policies load when init a server, in that policies are some validations like:
  * isLoggedIn or isAdmin
  * To use:
  *
  * var authPolicyIsLoggedIn      = Cobuild.Policies.getPolicy('cobuild.'+config.authentication.strategy+'.isLoggedIn');
  * var authPolicyIsAdmin      = Cobuild.Policies.getPolicy('cobuild.'+config.authentication.strategy+'.isAdmin');
  *
  * On the declaretion of end-point call a policy
  *
  * router.put('/', authPolicyIsLoggedIn, Ctrl.update);
  *
  */

  router.get('/health', function(req, res) {

     return res.status(200).send('ok');
  });

  /**
  * @api {get} /api/v1/reports/risk/:survey/:report  Risk report PDF
  * @apiName Core -  Risk report PDF
  * @apiGroup reports
  * @apiDescription  Risk report PDF
  * @apiVersion 1.0.0
  *
  * @apiParam {integer} surver id of the survey
  * @apiParam {integer} report id of the report
  *
  * @apiSuccessExample {Buffer} Response-Example: 
  * {
  *   "type": "buffer",
  *   "data": [
  *     45,
  *     50,
  *     50,
  *     60,
  *     80,
  *     100
  *   ]
  * }
  * 
  */

  router.get('/risk/:lang/:survey/:report',  signedUrlUtil.verifier(), reports.generateReport);
  
  /**
  * @api {post} /api/v1/reports/risk/signed-url  Generate a signed url
  * @apiName Core -  Generate a signed url
  * @apiGroup reports
  * @apiDescription  Generate a signed url to download the report
  * @apiVersion 1.0.0
  * @apiParamExample {JSON} Request-Example:
  * {
  *   "service": "risk",
  *   "survey": "1112"
  * }
  * 
  * @apiSuccessExample {JSON} Response-Example: 
  * {
  *   "signed_url": "http://0.0.0.0:443/api/v1/report/risk/1201.7S8OkArw0XIaRZXcgUAunLcQ?module=report&signed=e:1543335337;r:7539385559;83a445ec04558244077453ebb75b42fd"
  * }
  * 
  */
 
  router.post('/risk/signed-url', reports.signedUrl); 

 /**
  * @api {post} /api/v1/reports/  Create report
  * @apiName Core -  Create report
  * @apiGroup reports
  * @apiDescription  Create report
  * @apiVersion 1.0.0
  * @apiParamExample {JSON} Request-Example:
  * {
  *   "survey": 12348
  * }
  * 
  * @apiSuccessExample {JSON} Response-Example: 
  * {
  *   "uid": "ALdZZ3rLwVawhcwQa7j8sHXH",
  *   "id": 249,
  *   "survey": 1319,
  *   "created_at": "2018-12-06 16:22:23.395644+00",
  *   "user": "5c094ca22b3ef20025033f8c"
  * }
  * 
  */

  router.post('/', reportsValidator.createReport ,reports.createReport);

  /**
  * @api {post} /api/v1/reports/risk/signed-url  Send pdf report by email
  * @apiName Core - Send pdf report by email
  * @apiGroup reports
  * @apiDescription Send pdf report by email
  * @apiVersion 1.0.0
  * @apiParamExample {JSON} Request-Example:
  * {
  *   "service": "risk",
  *   "survey": "1112",
  *   "uid": "HZREQHWoCgfOS1OsYyfsiXDl"
  * }
  * 
  * @apiSuccessExample {JSON} Response-Example: 
  * {
  *   "mail":{
  *     "statusCode": "200"
  *   },
  *   "report":{
  *     "buffer": {
  *       "type": "Buffer",
  *       "data":  [37, 80, 68, 70, 45, 49, 46, 52, 10, 49, 32, 48, 32, 111, 98, 106, 10, 60, 60, 10, 47, 84, 105, 116,…]
  *     }
  *   },
  *   "user":{
  *     "id": 454,
  *     "email": "doe@rokk3rlabs.com"
  *   }
  * }
  * 
  */
  router.post('/risk/send-report', reports.sendReport);
  
  console.log('/api/'+Cobuild.config.apiVersion+'/report');
  app.use('/api/'+Cobuild.config.apiVersion+'/report',router);
}