var Cobuild                     = require('cobuild2');
var _                           = require('lodash');
var reportService               = require(Cobuild.Utils.Files.dirpath(__dirname)+'/services/reportService');

var exports                     = module.exports;


exports.generateReport = function(req, res) {

  reportService
  .generateReport(req)
  .then((response)=>{
    res.attachment(`report-${response.survey}.pdf`)
    res.send(new Buffer(response.buffer, 'binary'))
  })
  .catch((err)=>{
    res.badRequest(err);
  })

}


exports.signedUrl = function(req, res){
  var uid = _.get(res, 'locals.identity.uid');
  reportService
  .signedUrl(req, uid)
  .then((response)=>{
    res.ok(response)
  })
  .catch((err)=>{
    res.badRequest(err);
  })
};


exports.createReport = function(req, res){
  var uid = _.get(res, 'locals.identity.uid');
  reportService
  .createReportFn(req, uid)
  .then((response)=>{
    res.ok(response)
  })
  .catch((err)=>{
    res.badRequest(err);
  })
}


exports.sendReport = function(req, res){
  var uid = _.get(res, 'locals.identity.uid');
  reportService
  .sendReport(req, uid)
  .then((response)=>{
    res.ok(response)
  })
  .catch((err)=>{
    res.badRequest(err);
  })
}

