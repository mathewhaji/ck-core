const async           = require('async');
const i18n            = require('i18n');
const Cobuild         = require('cobuild2');
const _               = require('lodash');
const moment          = require('moment');

const utilsMail       = Cobuild.Utils.Files.requireIfExists('/app/utils/mail');

var exports           = module.exports;

exports.sendReport = function(user, doc, report){
  return new Promise(function(resolve, reject){
    const payload = {
      to: user.email,
      subject: i18n.__('report_subject', {
        company: report.companyName
      }),
      templateName: 'template',
      templateContent: 'template',
      templateVars: {         
        message: [
          i18n.__('report_message', {
            company: report.companyName,
            date: moment(report.createdAt).format('DD.MM.YYYY')
          }),
          i18n.__('farewell')
        ],
        details: [
          i18n.__('report_id', {
            uid: report.uid
          })
        ]
      },
      attachment: {
        data: doc.buffer,
        filename: `report-${doc.survey}.pdf`
      }
    };

    utilsMail.sendMail(payload)
    .then(response=>resolve(response))
    .catch(err=>reject(err));
  });
}