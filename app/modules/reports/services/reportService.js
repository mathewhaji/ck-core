var Cobuild         = require('cobuild2');
var _               = require('lodash');
var async           = require('async');
var request         = require("request")
var config          = Cobuild.config;
var moment          = require('moment');
var uid             = require('uid2');
var i18n            = require('i18n');

var Report          = require(Cobuild.Utils.Files.dirpath(__dirname)+'/models/report');
var mailService     = require(Cobuild.Utils.Files.dirpath(__dirname)+'/services/mailService');
var riskService     = Cobuild.Utils.Files.requireIfExists('/app/modules/risk/services/riskService');
var categoryService = Cobuild.Utils.Files.requireIfExists('/app/modules/category/services/categoryService');
var signedUrlUtil   = Cobuild.Utils.Files.requireIfExists('/app/utils/signedUrl');
var userService     = Cobuild.Utils.Files.requireIfExists('/app/modules/user/services/userService');

const categoriesTitle = [
  {
    title: 'Data Breach',
    translate: 'data-breach'
  },
  {
    title: 'Insider Threat',
    translate: 'insider-threat'
  },
  {
    title: 'GDPR',
    translate: 'gdpr'
  },
  {
    title: 'Software Vulnerabilities',
    translate: 's-w-vuln'
  },
  {
    title: 'DDoS',
    translate: 'ddos'
  },
  {
    title: 'E-Theft',
    translate: 'e-theft'
  }
]

function generateReport(req) {
  const lang = req.params.lang
  i18n.setLocale( lang ? lang : 'en');
  return new Promise((resolve, reject) => {
    var requiredData = ['survey', 'report'];
    var emptyData = [];

    requiredData.forEach((required)=>{
      if(!req.params[required]) 
        emptyData.push(required)
    })

    if(!_.isEmpty(emptyData))
      return reject(`${emptyData.toString()} can not be empty`)

    var getResults = function(cb) {
      req.params.id = req.params.survey;
      riskService.results(req)
      .then((response)=>{
        cb(null, response);
      })
      .catch(cb)
    }

    var getCategories = function(cb){
      categoryService.list({query: {}, params: {}})
      .then((response)=>{
        cb(null, response.data)
      })
      .catch(cb)
    }

  
    var formatPayload = ['risk', 'categories', function(cb, cbData){
      const risk = cbData.risk;
      const categories = cbData.categories;
      const levels  = { 'L': 'Low', 'M': 'Medium', H: 'High' }
      let categoryData = []
      risk.categories.forEach((val, index) => {

        const cat =  _.find(categories, { 'slug': val.category });
        const name = _.find(categoriesTitle, { 'title': cat.name })
        categoryData.push({
          name: i18n.__(name.translate),
          description: i18n.__(cat.description),
          score: val.score.value,
          level: levels[val.score.level]
        })
      });
      let risksByCategory = [];

      const groupedData = _.groupBy(categoryData, function(category) {
        return category.level;
      });

      for (let key in groupedData) {
        risksByCategory.push({
          risk: i18n.__(key),
          order: key  === 'High' ? 1 : key  === 'Medium' ? 2 : 3,
          class: key  === 'High' ? 'high' : key  === 'Medium' ? 'medium' : 'low',
          categories: groupedData[key]
        })
      }

      risksByCategory = _.orderBy(risksByCategory, ['order'], ['asc']);

      let payload = {
        hey: i18n.__('hey'),
        initialReport: i18n.__('initial-report'),
        scoreTitle: i18n.__('your-score'),
        majorFactorsTitle: i18n.__('major-factors'),
        factors: i18n.__('factors'),
        impact: i18n.__('estimated-impact'),
        page: i18n.__('page'),
        of: i18n.__('of'),
        risks: risksByCategory,
        company: risk.company,
        date: moment().format('DD.MM.YYYY'),
        score: risk.risk.score,
        uid: req.params.report,
      };

      cb(null, payload);
    }];

    var getReport = ['payload', function(cb, cbData){
      var payload = cbData.payload;
      let req = {
        url: 'http://reports:1337/api/v1/pdf',
        json: true,
        method: 'POST',
        body: {
          template: 'report-template-risk-v4.1.html',
          payload: payload
        }
      }
      
      request(req, (err, response, body)=>{
       
        cb(err, body.data);
      })
    }]

    var tasks = {
      risk: getResults,
      categories: getCategories,
      payload: formatPayload,
      file: getReport
    }
  
    async.auto(tasks, (err, response)=>{
      if(err) return reject(err);
  
      resolve({
        'buffer': response.file.buffer,
        'survey': req.params.id,
        'stream': response.file.stream
      })
    })
  });
}

function createReportFn(req, id){
  return new Promise((resolve, reject) => {

    var payload = {
      survey: parseInt(req.body.survey, 10),
      uid: uid(24),
      user: id
    };

    Report('report')
    .insert(payload,'*')
    .then((result)=> {
      
      let report = result.shift()
      resolve(report)
    })
    .catch((err)=>{
      reject(err);
    });
  });
}

function findReportFn(req, id){
  return new Promise((resolve, reject) => {

    var payload = {
      survey: parseInt(req.body.survey),
      user: id
    };


    Report
    .select('uid')
    .from('report')
    .where(payload)
    .first()
    .then((result)=> {
      console.log(result);
      resolve(result)
    })
    .catch((err)=>{
      reject(err);
    });
  });
}

function signedUrl(req, id){
  const lang = req.get('Accept-Language');
  return new Promise((resolve, reject) => {
    
    var findReport = function(cb){
      if(req.body.uid)
        return cb(null, req.body)

      findReportFn(req, id)
      .then((response)=>{
        cb(null, response)
      })
      .catch(cb)
    }

    var signedUrlFn = function(cbData, cb){
      var report  = cbData.uid;
      var service = req.body.service;
      var survey  = req.body.survey;

      req.body = {
        'service': `${service}.${lang}.${survey}.${report}`,
        'module': 'report'
      }

      signedUrlUtil.signed(req, cb)
    }

    var tasks = [
      findReport,
      signedUrlFn
    ]

    async.waterfall(tasks, (err, response)=>{
      if(err) return reject(err);

      resolve(response)
    })
  })
};

function sendReport(req, uid){

  return new Promise((resolve, reject) =>{ 
    var getUser = function(cb){
      userService
      .assignedUser(uid)
      .then((response)=>{
        cb(null, response)
      })
      .catch(cb)
    }

    var generate = function(cb){
      req.params.survey = req.body.survey;
      req.params.report = req.body.uid;
      generateReport(req)
      .then((response)=>{
        cb(null, response)
      })
      .catch(cb)
    }

    var sendMail = ['user', 'report' ,function(cb, prevData){
      const report = req.body;
      const user = prevData.user;
      const doc = prevData.report;

      mailService.sendReport(user, doc, report)
      .then(response=>cb(null, response))
      .catch(cb);
    }]

    var tasks = {
      user: getUser,
      report: generate,
      mail: sendMail
    }
    
    async.auto(tasks, (err, response)=>{
      if(err) return reject(err)

      resolve(response)
    })
  })
}

module.exports = {
  generateReport,
  signedUrl,
  sendReport,
  createReportFn
};