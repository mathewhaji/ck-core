var Cobuild           = require('cobuild2');
var express           = require('express');
var router            = express.Router();
var authController   = require(Cobuild.Utils.Files.dirpath(__dirname)+'/controllers/authController');
var authValidator    = require(Cobuild.Utils.Files.dirpath(__dirname)+'/validators/auth');

module.exports = function(app, limiter) {
  
/**
  *   Limiter
  *     For more info see: https://www.npmjs.com/package/express-limiter
  */
  if (limiter){
    // Here insert the rules for use limiter.
    // Example
    limiter({
      path: '/api/'+Cobuild.config.apiVersion+'/categories/health',
      method: 'get',
      lookup: ['connection.remoteAddress'],
      // 10 requests per 10 seconds
      total: 10,
      expire: 1000 * 10 ,
      onRateLimited: function (req, res, next) {
        next({ message: 'Rate limit exceeded', status: 400 })
      }
    });
  };
  
/**
  * The Policies load when init a server, in that policies are some validations like:
  * isLoggedIn or isAdmin
  * To use:
  * 
  * var authPolicyIsLoggedIn      = Cobuild.Policies.getPolicy('cobuild.'+config.authentication.strategy+'.isLoggedIn');
  * var authPolicyIsAdmin      = Cobuild.Policies.getPolicy('cobuild.'+config.authentication.strategy+'.isAdmin');
  * 
  * On the declaretion of end-point call a policy
  *
  * router.put('/', authPolicyIsLoggedIn, Ctrl.update);
  *
  */
  
  router.get('/health', function(req, res) {
    
     return res.status(200).send('ok');
  });

/**
 * @api {post} /api/v1/auth/login Login
 * @apiName Auth - Login
 * @apiGroup Auth
 * @apiDescription Login flow
 * @apiVersion 1.0.0
 *
 *
 * @apiParamExample {json} Request-Example:
 * {
 *   "email": "deo@rokk3rlabs.com",
 *   "password": "trudat55!"
 * }
 *
 * @apiSuccessExample {JSON} response-example:
 * {
 * 
 *   "auth": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJ1aWQiOiI1YzA1OTk0M2Y3NDhiOTAwMjFlYTc5MzMifQ.LwV9aaOerQHEgGcI0dQdxTRC9ygHtoZlhJRCucjYi0n4kd-8A5zSWCDLJe2F40WXEJioYn7yY0BNvGJBISSWjK_joN4wy4ZwLLjAUTQ_v2TFsfMbv-Bsl8eUxsYCDz7RK4pW_5zisNIzKXThZTP88khMQRLv9ksZQMVBZQCdPsTt9q8RLZR87ecfxaSQICxhcKtfsf0hwBYZVQQXqimWZ-gvu-whYFWSWTQiTYYofAgo6spqAtw-midnQd3ZLm-FIx2nbp6H9tjV5U9MEL3F76zHzbse41L4Mx4TQO5z27VmcKXopWs1mv4zucep4-o2V3cjbtS8bQ0me02h-18UpI3afp4NTs_zso2QSaJsjJ2nKVpinb-VYK1nvJEdJxjMBA-D0KL68zpxbTPzUoHTImTDXDvcmX5N-rfScenfur_hclEGkPFbHXzyam0g2bhr7zb247_PxCs3UOVctVG43t-Y2o_DWTVjfAjR4MOhLkAk72DJQJBp90V9IrBxyPqMIdJMuUUykKwrLSgxqde5bM6Bv314QOH6e1ylPFKc9dbD2aFssR3R4YYZy95kA5JHSCCY6XVUrypriRpAPO-3UCHv1Wy6K1pfeE-zJl10e0uGwJKWMiewOWS2ZFoE6avpeY8LhOvoF2Es0-67wsWEAI68sPc_VXQqXieMyQe28Mo",
 *   "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJjb2J1aWxkLWNsdXN0ZXItY29tcG9zZSIsImV4cCI6MTU0Mzg3MzAwOTI3NSwiYWNjb3VudF9pZCI6IjVjMDU5OTQzNGRhZTVhMDAxMDc3NWEyYSIsImNsaWVudF9pZCI6ImF1dGgtY29tcG9zZSIsImlhdCI6MTU0Mzg3MTIwOX0.AwpKfnFcDkUPZ05-t0emGoiDsEtmz9JQuZJd_hBj25C3GrlA-ZQIFhXUaLK0IZBpwQ2yBYUi_o4J7IdtemNiuskkILBwh40__ttdeq7_yf7i-f6OdUM3Dl7jIedJbx2eyzENm3kt7kP0TbtkG3gL-sU_H6ikmng-KZ14QwUlZWlX9IuCZGYd6XyIiiR1rZZNjPu9hgapXauKE-0RoZwrnwRs01YAeet8BgjmAlBqFoka9QWRr9eEoEy-k8tyzITT1LFKtb6E3acA1GqeT0AsypYJd5jXHvi7tPypBgLcQ-x5aX79pSm0FfoypC-gpCVdpuObaGj8fE1ILMpmQ2vMlozgfbxIVqnWO2rHBQsHmbZFWPmCPBepmyppRHwiWs6ZlETssaby62SWAbfAjC6TrvLdDsSQDGgQD4F3iL2erjgXz2GTVPQWE6SrWzXrz00iVJ7-YOkASkG4lEWL8I5XQfd-04AYFFCoKXN7kI65UlSqaF_eBk_bgwfXtXo-VPWCjHTSGNhJ3jBaptvipauUiMQ9nG9MFMiMUBwHJ4ucISWF4lzDD0b43JZC2VPzTC0Xn84hjbWCMFNJquBvZh2Phb1RQtCnGlaqRfeUSivsC80k5nu2bOJfubyNWqp8sEEXDLOP9-E_Ync6fF4BehvfhyNLJLhscU-Gp62W0Zu_-f8",
 *   "redirect": "/users"
 * }
 */   
  router.post('/login', authValidator.login, authController.login);

  app.use('/api/'+Cobuild.config.apiVersion+'/auth',router);
  
};
