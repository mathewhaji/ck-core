var Cobuild                     = require('cobuild2');
var _                           = require('lodash');
var authService                 = require(Cobuild.Utils.Files.dirpath(__dirname)+'/services/authService');

var exports                     = module.exports;

exports.login = function(req, res) {
  
  authService.login(req.body)
  .then(function(record) {
    res.ok(record)
  })
  .catch(function(err){
    res.badRequest(err);
  });

};
