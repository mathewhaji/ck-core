var _       = require('lodash');
var exports = module.exports;

exports.login = function (req, res, next) {
  var requiredData = ['email', 'password'];
  var emptyData = [];
  var data = req.body;

  requiredData.forEach((key)=>{
    if(!data[key]) emptyData.push(key)
  })
  
  if(!_.isEmpty(emptyData))
    return res.badRequest({error:'invalid-record'});
  
  next();
}