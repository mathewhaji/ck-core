var Cobuild           = require('cobuild2');
var _                 = require('lodash');
var async             = require('async');
var userService       = Cobuild.Utils.Files.requireIfExists('/app/modules/user/services/userService');
var i18n              = require('i18n');
var request           = require('request-promise');
var config            = Cobuild.config;

var exports           = module.exports;



exports.login = function(data){
  return new Promise(function(resolve, reject){
    var tokenType = '';
    var accessToken = '';
    var user = {};


    var getPolicies =  (cb)=>{
      userService
      .getUid(data.email)
      .then((response)=>{
        if(!response) return cb(i18n.__('user_not_found'))

        user = response;
        return userService.getPolicies(response.uid)
      })
      .then((policies)=>{
        cb(null, policies)
      })
      .catch(cb)
    };


    var defineRedirect = ['policies', (cb, cbData)=>{
      var policies = cbData.policies || [];
      var redirect = '';

      var refs = {
        '*': '/dashboard',
        'DashboardAdmin': '/admin-dashboard',
        'DashboardUser': '/user-dashboard'
      }

      policies.forEach(function(value){
        value.forEach(function(policy){
          if(refs[policy]){
            return redirect = refs[policy];
          }
        })
      })

      cb(null, redirect);
    }]
    
    var activeAccount = ['policies', (cb, cbData)=>{
      if(user && user.active)
        return cb(null, user.active)
      
      cb(i18n.__('user_is_not_activated'))
    }]

    var authResponse = (cb)=>{
      var payload = {
        client_id     : config.authServer.clientId,
        client_secret : config.authServer.clientSecret,
        grant_type : 'password',
        password: data.password,
        username: data.email,
        email: data.email
      }
  

      var options = {
        url: 'http://auth:1337/api/v1/oauth/token',
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        form: payload
      }

      request(options)
      .then((response)=>{
        response = JSON.parse(response);
        tokenType = response.token_type;
        accessToken = response.access_token;
        cb(null, response);
      })
      .catch(cb)

    }

    var authorizeResponse = ['auth', (cb, cbData)=>{


      var auth = cbData.auth;
      var payload = {
        client_id     : config.authServer.clientId,
        client_secret : config.authServer.clientSecret,
        state: 'state',
        scope: 'red',
        response_type: 'code',
        redirect_uri: `${config.BE.host}/iam/v1/identities/oauth/code`
      };

     

      var options = {
        url: 'http://auth:1337/api/v1/oauth/authorize',
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': `${auth.token_type} ${auth.access_token}`
        },
        form: payload
      };

      request(options)
      .then((response)=>{
        var uriPart = /http.*$/;
        if (response && response.match(uriPart)) {
          var redirectUri = response.match(uriPart);
          return cb(null, {
            url: redirectUri ? redirectUri[0]: ''
          });
        }
     
        response = response.json() || {};
        cb(null, {
          token: response.token,
          token_type: response.token_type
        }) 
      })
      .catch(cb)
    }]

    var iamResponse = ['authorize', (cb, cbData)=>{
      var authorize = cbData.authorize;
      if(!authorize.url)
        return cb(null, authorize);

      authorize.url = authorize.url.split('/iam')[1]
      var options = {
        url: `http://iam:1337/api${authorize.url}`, 
        method: 'GET'
      };

      request(options)
      .then((response)=>{
        var data = JSON.parse(response) || {};
        
        cb(null, {
          token: data.data.token,
          token_type: tokenType
        })
      })
      .catch(cb)

    }]

    var formatResponse = ['iam', 'active', 'redirect', (cb, cbData)=>{
      var iam = cbData.iam;
      var redirect = cbData.redirect;
      cb(null, {
        auth: iam.token,
        access_token: accessToken,
        redirect: redirect
      })
    }]

    var tasks = {
      policies: getPolicies,
      redirect: defineRedirect,
      active: activeAccount,
      auth: authResponse,
      authorize: authorizeResponse,
      iam: iamResponse,
      format: formatResponse

    }

    async.auto(tasks, (err, response)=>{
      if(err) return reject(err);

      resolve(response.format)
    })
  })
};

