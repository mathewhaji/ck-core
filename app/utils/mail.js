const request         = require('request-promise');
var exports           = module.exports;

exports.sendMail      = (payload)=>{
  return new Promise(function(resolve, reject){
    request({
      url:'http://notifications:1337/api/v1/emails',
      method:'POST',
      body: payload,
      json: true
    })
    .then(response=>resolve(response))
    .catch(err=>reject(err));
  });
}