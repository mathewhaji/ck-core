var Cobuild           = require('cobuild2');
var _                 = require('lodash');
var config            = Cobuild.config;
var signed            = require('signed');

var signature         = signed({
  secret: [config.signedUrl.secret],
  ttl: config.signedUrl.ttl
});

var exports           = module.exports;

exports.signed = function signedUrl(req, cb) {
  let required = ['service', 'module'];
  let empty    = [];
  let data = req.body;

  required.forEach((column)=>{
    if(!data[column])
      empty.push(column)
  })
  
  if(!_.isEmpty(empty))
    return cb(`${empty.toString()} can not be empty`);

  
  data.service = replaceAll(data.service, '.', '/')
  
  let host = `${req.protocol}://${req.get('host')}`;

  let url = `${host}/${data.service}`;

  let params = _.clone(data);

  delete params.version;
  delete params.service;

  let count = 0;

  _.forEach(params, (value, key) => {
    let param = '&';
    if (count === 0)
      param = '?';
    url += `${param}${key}=${value}`
    count++;
  })

  url = signature.sign(url);

  url = url.replace(host, `${host}/api/v1/${data.module}`);

  cb(null, {
    signed_url: url
  });
}

exports.verifier = function verifier(){
  return signature.verifier()
}

function replaceAll(input, search, replace){
  return input.split(search).join(replace)
}