var _ = require('lodash');
var fs = require('fs');

var defaultConfig = {
  'web': {
    'port': process.env.PORT || 3000,
    'host': process.env.HOST,
    'sessionSecret': process.env.SESSION_SECRET
  },
  'db': {
    'connectionString': process.env.DB_URL
  },
  'env':process.env.NODE_ENV,
  'surveyFormURL':process.env.SURVEY_FORM_URL || 'http://survey:1337/api/v1/surveys/form',
  'apiVersion':'v1',
  'authServer':{
    'clientId':process.env.CLIENT_ID || 'local',
    'clientSecret':process.env.CLIENT_SECRET || 'secret'
  },
  'iam':{
    'adminToken':process.env.ADMIN_TOKEN || 'token'
  },
  'defaultUser':{
    'email': process.env.DEFAULT_USER_MAIL || 'doe+cytrust@rokk3rlabs.com',
    'password': process.env.DEFAULT_USER_PASSWORD ||'trudat'
  },
  'jwt': {
    'secret': fs.readFileSync(process.env.IDENTITY_PUBLIC_KEY_PATH),
    'algorithm': process.env.ALGORITHM
  },
  'FE':{
    'host': process.env.HOST_FE
  },
  'BE':{
    'host': process.env.HOST_BE
  },
  'signedUrl':{
    'secret': process.env.SIGNED_URL_SECRET || 'secret',
    'ttl': process.env.SIGNED_URL_TTL || 3600
  },
  scores: {
    high: process.env.HIGH_SCORE || 10,
    medium: process.env.MEDIUM_SCORE || 20,
    low: process.env.LOW_SCORE ||  30,
  },
  'sentryKey': process.env.SENTRY_KEY || 'https://ae0d7ab620274109991ec43f6e04082f@sentry.io/1813534',
  'url_notification': process.env.URL_NOTIFICATION
};

module.exports = (function () {
  var envConfig = require('./env/'+ (process.env.NODE_ENV ? process.env.NODE_ENV : 'local')+'/config');
  var config = _.merge(defaultConfig, envConfig);

  console.log("config:",config);
  return config;

 })();
